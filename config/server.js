// config created by Omar Nawito
require('dotenv').config()

const dbHost = process.env.DB_HOST
const dbPort = process.env.DB_PORT
const dbName = process.env.DB_NAME
const dbUser = process.env.DB_USER || ''
const dbPass = process.env.DB_PASS || ''
const dbSource = process.env.DB_Source || ''

const dbCred =
    dbUser.length > 0 || dbPass.length > 0 ? `${dbUser}:${dbPass}@` : ''
const dbUrl =
    process.env.DB_URL || `mongodb://${dbCred}${dbHost}:${dbPort}/${dbName}?authSource=${dbSource}`
//?authSource=${dbSource}
module.exports = {
  apiListenPort: process.env.PORT || 3000,
  APP_URL: process.env.APP_URL,
  frontBaseUrl: '*',
  smtpServer: {
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    secure: process.env.MAIL_SECURE,
    user: process.env.MAIL_USERNAME,
    pass: process.env.MAIL_PASSWORD,
    fromName: process.env.MAIL_FROMNAME,
    fromAddress: process.env.MAIL_FROMADDRESS
  },
  jwtSecretKey: process.env.jwtSecretKey,
  mongodbServerUrl: dbUrl
}
