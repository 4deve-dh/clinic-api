const express = require('express')
const reservationController = require('../../controllers/doctor/reservation')
const isAuth = require('../../middleware/is-auth')

const router = express.Router()

router.put('/complete/:reservationId', isAuth, reservationController.completeReservation)
router.get('/', isAuth, reservationController.getAllReservedPatients)
router.get('/current-patient', isAuth, reservationController.getCurrentPatient)
router.patch('/history/:patientId', isAuth, reservationController.getAllPatientsHistory)

module.exports = router
