const express = require('express')
const attendnaceController = require('../../controllers/reception/attendance')
const isAuth = require('../../middleware/is-auth')

const router = express.Router()

router.post('/', isAuth, attendnaceController.makeAttendance)
router.patch('', isAuth, attendnaceController.getAllAttendances)

module.exports = router
