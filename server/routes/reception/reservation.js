const express = require('express')
const reservationController = require('../../controllers/reception/reservation')
const isAuth = require('../../middleware/is-auth')

const router = express.Router()

router.patch('/:branchId', isAuth, reservationController.getReservations)

router.post('/', isAuth, reservationController.makeReservation)

router.get('/:reservationId', isAuth, reservationController.getReservation)

router.get('/patient/:patientId', isAuth, reservationController.getReservationsByPatient)

router.post('/filter/:branchId', isAuth, reservationController.getReservationsFilter)

router.post('/statuses/all', isAuth, reservationController.getStatuses)

router.post('/:reservationId', isAuth, reservationController.changeStatus)

router.put('/:reservationId', isAuth, reservationController.updateReservation)

router.put('/complete/:reservationId', isAuth, reservationController.completeReservation)

router.post('/bill/:reservationId', isAuth, reservationController.createBill)

router.post('/bill/update/:billId', isAuth, reservationController.updateBill)

router.delete('/:reservationId', isAuth, reservationController.deleteReservation)

router.post('/goto/:reservationId', isAuth, reservationController.gotoDoctor)

module.exports = router
