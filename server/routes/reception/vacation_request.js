const express = require('express')
const vacationRequestController = require('../../controllers/reception/vacationRequest')
const isAuth = require('../../middleware/is-auth')

const router = express.Router()

router.post('/', isAuth, vacationRequestController.sendVacationRequest)
router.get('/', isAuth, vacationRequestController.getMyVacationsRequests)

module.exports = router
