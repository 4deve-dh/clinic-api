const express = require('express')
const serviceController = require('../../controllers/admin/service')
const isAuth = require('../../middleware/is-auth')
const multer = require('multer')

const router = express.Router()
const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/videos/services')
  },
  filename: (req, file, cb) => {
    cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname)
  }
})

let upload = multer({
  storage: fileStorage,
  limits: { fileSize: '1mb' }
}).single('videoFile')

router.patch('/:branchId', isAuth, serviceController.getServices)

router.post('/', isAuth, upload, serviceController.createService)

router.get('/:serviceId', isAuth, serviceController.getService)

router.put('/:serviceId', upload, isAuth, serviceController.updateService)

router.delete('/:serviceId', isAuth, serviceController.deleteService)

router.post('/search', isAuth, serviceController.searchService)

module.exports = router
