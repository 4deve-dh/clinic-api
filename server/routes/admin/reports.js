const express = require('express')
const reportsController = require('../../controllers/admin/reports')
const cronScheduleController = require('../../controllers/admin/cronSchedule')
const isAuth = require('../../middleware/is-auth')

const router = express.Router()

router.get('/dashboard/:branchId', isAuth, reportsController.dashboard)
router.post('/create/:branchId', isAuth, reportsController.createReport)
router.post('/filteredAttendance', isAuth, reportsController.getFilteredAttendance)
router.get('/get-notifications/:branchId', isAuth, reportsController.getNotification)
router.post('/salary-report/:branchId', isAuth, reportsController.salaryReport)

router.get('/details/:branchId', isAuth, cronScheduleController.details)

module.exports = router
