const express = require('express')
const attendnaceController = require('../../controllers/admin/attendance')
const cronScheduleController = require('../../controllers/admin/cronSchedule')
const isAuth = require('../../middleware/is-auth')
const isAdmin = require('../../middleware/admin')

const router = express.Router()

router.post('/', isAuth, attendnaceController.makeAttendance)
router.get('/', isAuth, cronScheduleController.getAI)

module.exports = router
