const express = require('express')
const statusController = require('../../controllers/admin/status')
const isAuth = require('../../middleware/is-auth')

const router = express.Router()

router.patch('/', isAuth, statusController.getstatuses)

router.post('/', statusController.createStatus)

router.get('/:statusId', isAuth, statusController.getStatus)

router.put('/:statusId', isAuth, statusController.updateStatus)

router.delete('/:statusId', isAuth, statusController.deleteStatus)

router.post('/search', isAuth, statusController.searchStatus)

module.exports = router
