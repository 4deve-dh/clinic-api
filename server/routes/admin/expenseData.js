const express = require('express')
const expenseDataController = require('../../controllers/admin/expenseData')
const isAuth = require('../../middleware/is-auth')
const isAdmin = require('../../middleware/admin')

const router = express.Router()

router.patch('/:branchId', isAuth,isAdmin, expenseDataController.getExpensesData)

router.post('/', isAuth, expenseDataController.createExpenseData)

router.get('/:expenseDataId', isAuth, expenseDataController.getExpenseData)

router.put('/:expenseDataId', isAuth, expenseDataController.updateExpenseData)

router.delete('/:expenseDataId', isAuth,isAdmin, expenseDataController.deleteExpenseData)

module.exports = router
