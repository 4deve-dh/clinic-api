const express = require('express')
const patientController = require('../../controllers/admin/patient')
const isAuth = require('../../middleware/is-auth')

const router = express.Router()

router.patch('/:branchId', isAuth, patientController.getPatients)

router.post('/', isAuth, patientController.createpatient)

router.get('/:patientId', isAuth, patientController.getPatient)

router.put('/:patientId', isAuth, patientController.updatepatient)

router.delete('/:patientId', isAuth, patientController.deletePatient)

router.get('/bills/:patientId', isAuth, patientController.getPatientBills)

router.post('/search/:branchId', isAuth, patientController.searchpatient)

module.exports = router
