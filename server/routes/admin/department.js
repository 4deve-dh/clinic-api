const express = require('express')
const departmentController = require('../../controllers/admin/department')
const isAuth = require('../../middleware/is-auth')
const isAdmin = require('../../middleware/admin')

const router = express.Router()

router.patch('/', isAuth,isAdmin, departmentController.getDepartments)

router.post('/', isAuth, departmentController.createDepartment)

router.get('/:departmentId', isAuth, departmentController.getDepartment)

router.put('/:departmentId', isAuth, departmentController.updateDepartment)

router.delete('/:departmentId', isAuth,isAdmin, departmentController.deleteDepartment)

router.post('/search', isAuth, departmentController.searchDepartment)

module.exports = router
