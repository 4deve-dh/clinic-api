const express = require('express')
const router = express.Router()
const userController = require('../../controllers/admin/user')
const isAuth = require('../../middleware/is-auth')
const multer = require('multer')

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/images/users')
  },
  filename: (req, file, cb) => {
    cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname)
  }
})
const fileFilter = (req, file, cb) => {
  console.log('multer', file);
  if (file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg') {
    cb(null, true)
  } else {
    cb(null, false)
  }
}

let upload = multer({
  storage: fileStorage,
  fileFilter: fileFilter
})

router.post('/', isAuth, upload.single('image'), userController.createUser)
router.put('/:userId', isAuth, upload.single('image'), userController.updateUser)
router.get('/:userId', isAuth, userController.getUser)
router.get('/all/:branchId', isAuth, userController.getAllUserBranch)
router.get('/doctors/:branchId', isAuth, userController.getAllDoctorsBranch)
router.get('/users/:branchId', isAuth, userController.getAllUsersBranch)
router.delete('/:userId', isAuth, userController.deleteUser)

module.exports = router
