const express = require('express')
const branchController = require('../../controllers/admin/branch')
const isAuth = require('../../middleware/is-auth')
const isAdmin = require('../../middleware/admin')

const router = express.Router()

router.patch('/', isAuth, branchController.getBranches)

router.post('/', isAuth, branchController.createBranch)

router.get('/:branchId', isAuth, branchController.getBranch)

router.put('/:branchId', isAuth, branchController.updateBranch)

router.delete('/:branchId', isAuth, branchController.deleteBranch)

router.post('/search', isAuth, branchController.searchBranch)

module.exports = router
