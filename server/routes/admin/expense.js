const express = require('express')
const expenseController = require('../../controllers/admin/expense')
const isAuth = require('../../middleware/is-auth')
const isAdmin = require('../../middleware/admin')

const router = express.Router()

router.patch('/:branchId', isAuth, expenseController.getExpenses)

router.post('/:branchId', isAuth, expenseController.createExpense)

router.get('/:expenseId', isAuth, expenseController.getExpense)

router.put('/:expenseId', isAuth, expenseController.updateExpense)

router.delete('/:expenseId', isAuth, expenseController.deleteExpense)

module.exports = router
