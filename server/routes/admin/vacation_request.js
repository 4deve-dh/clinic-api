const express = require('express')
const vacationRequestController = require('../../controllers/admin/vacationRequest')
const isAuth = require('../../middleware/is-auth')

const router = express.Router()

router.get('/', isAuth, vacationRequestController.getAllVacationRequests)

router.put('/:vacationRequestId', isAuth, vacationRequestController.acceptRejectVacations)

module.exports = router
