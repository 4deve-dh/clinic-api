const express = require('express')
const contractController = require('../../controllers/admin/contract')
const isAuth = require('../../middleware/is-auth')
const isAdmin = require('../../middleware/admin')

const router = express.Router()

router.patch('/', isAuth, contractController.getContracts)

router.post('/', isAuth, contractController.createContract)

router.get('/:contractId', isAuth, contractController.getContract)

router.get('/services/:contractId', isAuth, contractController.getContractServices)

router.put('/:contractId', isAuth, contractController.updateContract)

router.delete('/:contractId', isAuth, contractController.deleteContract)

module.exports = router
