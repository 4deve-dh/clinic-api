const express = require('express')
const roleController = require('../../controllers/admin/role')
const isAuth = require('../../middleware/is-auth')

const router = express.Router()

router.patch('/', isAuth, roleController.getRoles)

router.post('/', roleController.createRole)

router.get('/:roleId', isAuth, roleController.getRole)

router.put('/:roleId', isAuth, roleController.updateRole)

router.delete('/:roleId', isAuth, roleController.deleteRole)

router.post('/search', isAuth, roleController.searchRole)

module.exports = router
