const express = require('express')
const scheduleController = require('../../controllers/admin/schedule')
const isAuth = require('../../middleware/is-auth')
const isAdmin = require('../../middleware/admin')

const router = express.Router()

router.post('/', isAuth, scheduleController.createSchedule)

router.get('/:doctorId', isAuth, scheduleController.getSchedules)

router.put('/:scheduleId', isAuth, scheduleController.updateSchedule)

router.delete('/:scheduleId', isAuth, scheduleController.deleteSchedule)

module.exports = router
