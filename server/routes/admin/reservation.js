const express = require('express')
const reservationController = require('../../controllers/admin/reservation')
const isAuth = require('../../middleware/is-auth')
const isAdmin = require('../../middleware/admin')

const router = express.Router()

router.patch('/', isAuth, reservationController.getReservations)

module.exports = router
