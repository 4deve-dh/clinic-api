const express = require('express')
const contractServiceController = require('../../controllers/admin/contractService')
const isAuth = require('../../middleware/is-auth')
const isAdmin = require('../../middleware/admin')

const router = express.Router()

router.patch('/', isAuth, contractServiceController.getContractsServices)

router.post('/', isAuth, contractServiceController.createContractService)

router.get('/:contractServiceId', isAuth, contractServiceController.getContractService)

router.put('/:contractServiceId', isAuth, contractServiceController.updateContractService)

router.delete('/:contractServiceId', isAuth, contractServiceController.deleteContractService)

module.exports = router
