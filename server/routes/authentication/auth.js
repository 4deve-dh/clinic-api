const express = require('express')
const router = express.Router()
const { body } = require('express-validator/check')
const authController = require('../../controllers/authentication/auth')
const isAuth = require('../../middleware/is-auth')
const multer = require('multer')
const pify = require('pify')
const path = require('path')

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/images/users')
  },
  filename: (req, file, cb) => {
    let ext = path.extname(file.originalname);
    let filename = file.fieldname + "-" + Date.now() + ext;
    cb(null, filename)
  }
})
const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg') {
    cb(null, true)
  } else {
    cb(null, false)
  }
}

let upload = pify(multer({
  storage: fileStorage,
  fileFilter: fileFilter
}).single('image')
)

router.post('/login', [
  body('email').isEmail().withMessage('please enter a valid email.')
], authController.login)

router.post('/logout', isAuth, authController.logout)

router.post('/forget-password', authController.forgetPassword)

router.post('/verify-token', authController.verifyToken)

router.post('/update_profile', isAuth, upload, authController.updateProfile)

module.exports = router
