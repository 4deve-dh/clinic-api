const redis = require('redis');
const {RateLimiterRedis} = require('rate-limiter-flexible');


const redisClient = redis.createClient({
  host: 'localhost',
  port: 6379,
  enable_offline_queue: true
});

const rateLimiter = new RateLimiterRedis({
  redis: redisClient,
  keyPrefix: 'middleware',
  points: 10, // 10 requests
  duration: 1, // per 1 second by IP
  blockDuration: 120, // Block duration in store
  inmemoryBlockOnConsumed: 301, // If userId or IP consume >300 points per minute
  inmemoryBlockDuration: 120 // Block it for two minutes in memory, so no requests go to Redis
});

const rateLimiterMiddleware = (req, res, next) => {
  rateLimiter.consume(req.ip)
      .then(() => {
        next();
      })
      .catch(() => {
        res.status(429).send('Too Many Requests');
      });
};

module.exports = rateLimiterMiddleware;
