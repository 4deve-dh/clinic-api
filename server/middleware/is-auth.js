const jwt = require('jsonwebtoken')
const User = require('../models/user')
const settings = require('../lib/settings')

module.exports = (req, res, next) => {
  const authHeader = req.get('Authorization')
  if (!authHeader) {
    const error = new Error('Not authenticated.')
    error.statusCode = 401
    throw error
  }
  const token = authHeader.split(' ')[1]
  let decodedToken
  try {
    decodedToken = jwt.verify(token, settings.jwtSecretKey)
  } catch (error) {
    error.statusCode = 401
    throw error
  }
  if (!decodedToken) {
    const error = new Error('Not authenticated.')
    error.statusCode = 401
    throw error
  }
  User.findByToken((token)).then(user => {
    if (!user) {
      const error = new Error('jwt expired.')
      error.statusCode = 401
      throw error
    }
    req.userId = decodedToken._id
    req.user = user
    req.token = token
    next()
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}
