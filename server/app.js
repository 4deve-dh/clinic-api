require('v8-compile-cache')
require('./db/mongoose')
require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const https = require('https')
const helmet = require('helmet')
const winston = require('winston')
const logger = require('./lib/logger')
const settings = require('./lib/settings')
const responseTime = require('response-time')
const fs = require('fs')
// const rateLimiterRedisMiddleware = require('./middleware/rateLimiterRedis')
const jwt = require('jsonwebtoken')

const userRoutes = require('./routes/admin/user')
const authRoutes = require('./routes/authentication/auth')
const roleRoutes = require('./routes/admin/role')
const departmentRoutes = require('./routes/admin/department')
const reservationRoutes = require('./routes/admin/reservation')
const reservationReceptionRoutes = require('./routes/reception/reservation')
const vacationRequestRoutes = require('./routes/reception/vacation_request')
const vacationRequestAdminRoutes = require('./routes/admin/vacation_request')
const statusRoutes = require('./routes/admin/status')
const contractRoutes = require('./routes/admin/contract')
const contractServiceRoutes = require('./routes/admin/contractService')
const branchRoutes = require('./routes/admin/branch')
const patientRoutes = require('./routes/admin/patient')
const expenseRoutes = require('./routes/admin/expense')
const expenseDataRoutes = require('./routes/admin/expenseData')
const scheduleRoutes = require('./routes/admin/schedule')
const serviceRoutes = require('./routes/admin/service')
const reportsRoutes = require('./routes/admin/reports')
const attendanceRoutes = require('./routes/reception/attendance')
const doctorReservationRoutes = require('./routes/doctor/reservation')

const cron = require('node-cron')
const start = require('./controllers/admin/cronSchedule')
let cors = require('cors')

if (process.env.NODE_ENV === 'development')
  console.log(`Application Started: clinic server to ${settings.APP_URL}:${settings.apiListenPort}`)

// cron job start every month
cron.schedule('59 23 30 * *', () => {
  start.getAI()
})
const app = express()
// app.use(rateLimiterRedisMiddleware)
app.use(helmet())
app.use(helmet.hidePoweredBy())
app.use(cors())

app.use(bodyParser.urlencoded({
  extended: true,
  limit: '50mb'
})) // x-www-form-urlencoded

app.use(bodyParser.json({limit: '50mb'})) // application/json
app.use(logger.sendResponse)
app.use(responseTime())
app.use('/uploads/images', express.static('uploads/images'))
app.use('/uploads/videos', express.static('uploads/videos'))

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE')
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, x-auth')
  res.setHeader('Access-Control-Expose-Headers', 'x-auth')
  next()
})

app.use('/api/admin/user', userRoutes)
app.use('/api/admin/department', departmentRoutes)
app.use('/api/admin/reservation', reservationRoutes)
app.use('/api/reception/reservation', reservationReceptionRoutes)
app.use('/api/reception/vacation_request', vacationRequestRoutes)
app.use('/api/admin/vacation_request', vacationRequestAdminRoutes)
app.use('/api/admin/contract', contractRoutes)
app.use('/api/admin/contract_service', contractServiceRoutes)
app.use('/api/admin/service', serviceRoutes)
app.use('/api/admin/branch', branchRoutes)
app.use('/api/admin/patient', patientRoutes)
app.use('/api/admin/status', statusRoutes)
app.use('/api/admin/expense', expenseRoutes)
app.use('/api/admin/expense_data', expenseDataRoutes)
app.use('/api/admin/schedule', scheduleRoutes)
app.use('/api/admin/role', roleRoutes)
app.use('/api/admin/auth', authRoutes)
app.use('/api/admin/reports', reportsRoutes)
app.use('/api/admin/attendances', attendanceRoutes)

app.use('/api/doctor/reservations', doctorReservationRoutes)

app.use((error, req, res, next) => {
  const status = error.statusCode || 500
  const message = error.message
  const data = error.data
  res.status(status).json({
    message: message,
    data: data
  })
})

const options = {
  key: fs.readFileSync('/etc/nginx/storage/certificate/privkey.pem'),
  cert: fs.readFileSync('/etc/nginx/storage/certificate/fullchain.pem'),
  ca: fs.readFileSync('/etc/nginx/storage/certificate/chain.pem')
}

const server = https.createServer(options, app)
server.listen(settings.apiListenPort, () => {
  const serverAddress = server.address()
  winston.info(`API running at ${settings.APP_URL}:${serverAddress.port}`)
})

const io = require('./socket').init(server)
let users = []

io.use(function(socket, next){
  console.log(socket.handshake.query.token)
  if (socket.handshake.query && socket.handshake.query.token){
    jwt.verify(socket.handshake.query.token, settings.jwtSecretKey, function(err, decoded) {
      if(err) return next(new Error('Authentication error'));
      socket.decoded = decoded;
      next();
    });
  } else {
    next(new Error('Authentication error'));
  }
}).on('connection', socket => {
  console.log(socket.handshake.query)
  console.log(`User connected ${socket.id}`)
  console.log('handshake', socket.handshake.query["userId"])
  let userIndex = users.findIndex(u => u.userId == socket.handshake.query["userId"])
  if (userIndex > -1)
    users.splice(userIndex, 1)

  users.push({
    socket: socket,
    userId: socket.handshake.query["userId"]
  })

  socket.on('custom', data => socket.broadcast.emit('custom', data))

  socket.on('sendToDoctor', (data) => {
    console.log('data'.data)
    let participantSocket = users.find(u => u.userId == data.userId)
    if (participantSocket) {
      participantSocket.socket.emit('currentPatient', data)
    }
  })

  socket.on('sendToReceptions', (data) => {
    let participantSocket = users.find(u => u.userId == data.userId)
    if (participantSocket) {
      participantSocket.socket.emit('receptions', data)
    }
  })

  socket.on('patientReservation', (data) => {
    console.log('new reservation', data)
    let participantSocket = users.find(u => u.userId == data.userId)
    if (participantSocket) {
      participantSocket.socket.emit('doctor', data)
    }
  })

  socket.on('with-branch', function (branchID) {
    let roomID = branchID // create a unique room id
    socket.join(roomID) // make user  join with branch
    console.log(`joined to room ${roomID}`)
  })

  socket.on('disconnect', () => {
    console.log('User disconnected from server')
  })
})
