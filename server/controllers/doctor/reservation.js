const _ = require('lodash')
const Reservation = require('../../models/reservation')
const ReservationService = require('../../models/reservationService')
// const Patient = require('../../models/patient')
const Joi = require('joi')
const moment = require('moment')
Joi.objectId = require('joi-objectid')(Joi)
const io = require('../../socket')

exports.completeReservation = async (req, res, next) => {
  const schema = {
    complain: Joi.string().allow('').allow(null),
    diagnosis: Joi.string().allow('').allow(null),
    notes: Joi.string().allow('').allow(null),
    services: Joi.array()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const reservationId = req.params.reservationId
  const body = _.pick(req.body, ['complain', 'diagnosis', 'notes', 'services'])

  Reservation.findByIdAndUpdate(reservationId, {
    complain: body.complain,
    diagnosis: body.diagnosis,
    notes: body.notes,
    status: '5cb8c2b9bfaae1089bda11e2'
  }, {new: true}).then(async reservation => {
    if (!reservation || reservation.doctor != req.userId) {
      const error = new Error('Could not find reservation.')
      error.statusCode = 404
      throw error
    }
    await ReservationService.find({reservation: reservationId}).deleteMany()
    if (body.services) {
      body.services.forEach(service => {
        let resServ = new ReservationService({
          reservation: reservationId,
          service: service.service,
          price: service.price,
          discount: service.discount
        })
        resServ.save()
      })
    }
    return reservation.save()
  }).then(reservation => {
    res.status(200).json({
      message: 'reservation updated!',
      data: reservation
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getAllReservedPatients = (req, res, next) => {
  Reservation.find({
    doctor: req.userId,
    isDeleted: false,
    status: '5c8e1fc11c5c323f54e0a802',
    date: {
      $gte: moment().startOf('day').toDate(),
      $lte: moment().endOf('day').toDate()
    }
  }).populate('patient').then((reservations) => {
      res.status(200).json({
        message: 'all reservations fetched successfully',
        data: reservations
      })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getAllPatientsHistory = (req, res, next) => {
  const patientId = req.params.patientId
  Reservation.find({patient: patientId}).then((reservations) => {
    if(reservations.length){
      Reservation.populate(reservations, 'doctor').then(() => {
        res.status(200).json({
          message: 'all reservations fetched successfully',
          data: reservations
        })
      }).catch(err => {
        if (!err.statusCode) {
          err.statusCode = 500
        }
        next(err)
      })
    }else {
      const error = new Error('Could not find reservation.')
      error.statusCode = 404
      throw error
    }
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getCurrentPatient = (req, res, next) => {
  Reservation.findOne({
    doctor: req.userId,
    isDeleted: false,
    status: '5c8e1fd01c5c323f54e0a804',
    date: {
      $gte: moment().startOf('day').toDate(),
      $lte: moment().endOf('day').toDate()
    }
  }).populate('patient').then(async (reservation) => {
      if(!reservation){
        const error = new Error('no reservation found')
        error.statusCode = 404
        throw error
      }
    let services = await ReservationService.find({ reservation: reservation._id }).populate('service')
        return res.status(200).json({
          message: 'reservation Services fetched',
          data: {
            reservation,
            services
          }
        })
      }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}
