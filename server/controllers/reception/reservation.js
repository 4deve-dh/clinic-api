const _ = require('lodash')
const Reservation = require('../../models/reservation')
const Status = require('../../models/status')
const ReservationService = require('../../models/reservationService')
const Bill = require('../../models/bill')
const Joi = require('joi')
const moment = require('moment')
Joi.objectId = require('joi-objectid')(Joi)

exports.getReservations = async (req, res, next) => {
  const currentPage = parseInt(req.query.page) || 1
  const perPage = 10

  let showAll = req.body.all
  const branchId = req.params.branchId

  let start, end

  if (req.body.type === 'today') {
    start = moment().startOf('day').toDate()
    end = moment().endOf('day').toDate()
  } else {
    start = moment().startOf('month').toDate()
    end = moment().endOf('month').toDate()
  }

  let totalCount = await Reservation.find({
    branch: branchId,
    ...(showAll == true ? {} : { isDeleted: false }),
    date: {
      $gte: start,
      $lte: end
    }
  }).countDocuments()

  Reservation.find({
    branch: branchId,
    ...(showAll == true ? {} : { isDeleted: false }),
    date: {
      $gte: start,
      $lte: end
    }
  }).sort({ createdAt: -1 }).skip((currentPage - 1) * perPage).limit(perPage)
    .populate([{ path: 'patient', populate: 'contract' }, 'doctor', 'user', 'status'])
    .then(async (reservations) => {
      let resvs = []

      for (let [index, reservation] of reservations.entries()) {
        let servicesCount = await ReservationService.countDocuments({ reservation: reservation._id })
        let add = true
        reservation.date = moment(reservation.date).format('YYYY-MM-DD')
        if (showAll == true && reservation.isDeleted) {
          let bill = await Bill.findOne({ reservation: reservation._id })
          if (!bill) add = false
        }

        if (add) {
          resvs.push({
            reservation,
            servicesCount
          })
        }

        if (index == reservations.length - 1) {
          return res.json({
            message: 'Fetched reservations successfully',
            data: resvs,
            total: reservations.length,
            meta: {
              total: totalCount,
              pages: Math.ceil(totalCount / perPage),
              perPage,
              currentPage
            },
          })
        }
      }

      if (reservations.length == 0) {
        return res.json({
          message: 'Fetched reservations successfully2',
          data: []
        })
      }
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500
      }
      next(err)
    })
}

exports.getReservationsFilter = async (req, res, next) => {
  const currentPage = parseInt(req.query.page) || 1
  const perPage = 10
  let showAll = req.body.all

  const schema = {
    filterName: Joi.string().allow(''),
    filterNumber: Joi.string().allow(''),
    filterMobile: Joi.string().allow(''),
    patient: Joi.objectId(),
    doctor: Joi.objectId(),
    dates: Joi.array().items(Joi.date().required(), Joi.date().required()),
    all: Joi.boolean().optional()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['filterName', 'filterNumber', 'filterMobile', 'patient', 'doctor', 'dates', 'all'])
  const branchId = req.params.branchId
  let filter = {}

  filter.branch = branchId

  if (body.all != true) {
    filter.isDeleted = false
  }

  if (body.doctor) {
    filter.doctor = body.doctor
  }
  if (body.patient) {
    filter.patient = body.patient
  }
  if (body.dates) {
    if (moment(body.dates[0]).isSame(body.dates[1])) {
      body.dates[0] = moment(body.dates[0]).startOf('day').format()
      body.dates[1] = moment(body.dates[1]).endOf('day').format()
    }

    filter.date = {
      $gte: body.dates[0],
      $lte: body.dates[1]
    }
  }

  let totalCount = await Reservation.find(filter).countDocuments()

  Reservation.find(filter).sort({ createdAt: -1 }).skip((currentPage - 1) * perPage).limit(perPage)
    .populate([{ path: 'patient', populate: 'contract' }, 'doctor', 'user', 'status'])
    .then(async (reservations) => {
      let resvs = []

      for (let [index, reservation] of reservations.entries()) {
        let servicesCount = await ReservationService.countDocuments({ reservation: reservation._id })
        let add = true

        reservation.date = moment(reservation.date).format('YYYY-MM-DD')

        if (showAll == true && reservation.isDeleted) {
          let bill = await Bill.findOne({ reservation: reservation._id })
          if (!bill) add = false
        }

        if (body.filterName != '') {
          add = add && reservation.patient.name_ar.toLowerCase().indexOf(body.filterName.toLowerCase()) > -1
        }

        if (body.filterMobile != '') {
          add = add && reservation.patient.mobile == body.filterMobile
        }

        if (body.filterNumber != '') {
          add = add && reservation.patient.number == body.filterNumber
        }

        if (add) {
          resvs.push({
            reservation,
            servicesCount
          })
        }

        if (index == reservations.length - 1) {
          return res.json({
            message: 'Fetched reservations successfully',
            data: resvs,
            total: reservations.length,
            meta: {
              total: totalCount,
              pages: Math.ceil(totalCount / perPage),
              perPage,
              currentPage
            },
          })
        }
      }

      if (reservations.length == 0) {
        return res.json({
          message: 'Fetched reservations successfully2',
          data: []
        })
      }
    }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getStatuses = (req, res, next) => {
  Status.find().then(statuses => {
    res.status(200).json({
      message: 'Fetched statuses successfully.',
      data: statuses
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.changeStatus = async (req, res, next) => {
  const schema = {
    status: Joi.objectId().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const reservationId = req.params.reservationId
  try {
    const _status = await Status.findOne({
      _id: req.body.status
    })
    if (_status == null) {
      const error = new Error('Could not find status.')
      error.statusCode = 404
      throw error
    } else {
      Reservation.findByIdAndUpdate(reservationId, {
        status: req.body.status
      }).then((reservation) => {
        res.status(200).json({
          data: _status,
          type: 'success',
          message: 'changed successfully'
        })
      })
    }
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}

exports.getReservation = (req, res, next) => {
  const reservationId = req.params.reservationId
  Reservation.findById(reservationId).then(reservation => {
    if (!reservation) {
      const error = new Error('Could not find reservation.')
      error.statusCode = 404
      throw error
    }
    return Reservation.populate(reservation, ['patient', 'doctor', 'user', 'status'])
  }).then(async (reservation) => {
    let services = await ReservationService.find({ reservation: reservation._id }).populate('service')

    res.status(200).json({
      message: 'Reservation fetched successfully.',
      data: {
        reservation,
        services
      }
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.makeReservation = async (req, res, next) => {
  const schema = {
    patient: Joi.objectId().required(),
    doctor: Joi.objectId().required(),
    date: Joi.date().required(),
    time: Joi.string().required(),
    notes: Joi.string().allow(''),
    branch: Joi.objectId().required(),
    services: Joi.array()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['patient', 'doctor', 'date', 'time', 'notes', 'branch', 'services'])

  const reservationConflict = await Reservation.find({
    date: body.date,
    time: body.time
  })
  if (reservationConflict.length > 0) {
    return res.status(409).json({
      type: 'warning',
      message: 'Reservation conflict with another patient'
    })
  }

  const status = await Status.findOne().sort({ created_at: 1 })
  const reservation = await new Reservation({
    patient: body.patient,
    doctor: body.doctor,
    date: body.date,
    time: body.time,
    user: req.user._id,
    status: status._id,
    notes: body.notes,
    branch: body.branch
  })
  console.log('services out', body.services)

  if (body.services) {
    for (let service of body.services) {
      let resServ = new ReservationService({
        reservation: reservation._id,
        service: service._id,
        price: service.price,
        discount: service.discount
      })
      resServ.save()
    }
  }
  reservation.save().then((reservation) => {
    return Reservation.populate(reservation, ['patient', 'doctor', 'user', 'status'])
  }).then((reservation) => {
    res.status(201).json({
      message: 'Reservation created successfully',
      data: reservation
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateReservation = async (req, res, next) => {
  const schema = {
    patient: Joi.objectId().required(),
    doctor: Joi.objectId().required(),
    date: Joi.date().required(),
    time: Joi.string().required(),
    notes: Joi.string().allow(''),
    branch: Joi.any(),
    services: Joi.array()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const reservationId = req.params.reservationId
  const body = _.pick(req.body, ['patient', 'doctor', 'date', 'time', 'notes', 'services'])

  const reservationConflict = await Reservation.find({
    _id: {
      $ne: reservationId
    },
    date: body.date,
    time: body.time
  })
  if (reservationConflict.length > 0) {
    return res.status(409).json({
      type: 'warnning',
      message: 'Reservation conflict with another patient'
    })
  }

  Reservation.findById(reservationId).then(reservation => {
    if (!reservation) {
      const error = new Error('Could not find reservation.')
      error.statusCode = 404
      throw error
    }
    reservation.patient = body.patient
    reservation.doctor = body.doctor
    reservation.date = body.date
    reservation.time = body.time
    reservation.notes = body.notes
    return reservation.save()
  }).then(async reservation => {
    if (body.services) {
      console.log('body.services', body.services)
      await ReservationService.find({ reservation: reservationId }).deleteMany()
      for (let service of body.services) {
        let resServ = new ReservationService({
          reservation: reservation._id,
          service: service._id,
          price: service.price,
          discount: service.discount
        })
        resServ.save()
      }
    }
    return reservation
  }).then((reservation) => {
    res.status(200).json({
      message: 'reservation updated!',
      data: reservation
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.deleteReservation = async (req, res, next) => {
  const reservationId = req.params.reservationId
  Reservation.findOneAndUpdate({
    _id: reservationId,
    isDeleted: false
  }, {
    $set: {
      isDeleted: true
    }
  }, { new: true }).then(() => {
    res.status(200).json({
      message: 'Deleted reservation successfully.'
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.completeReservation = async (req, res, next) => {
  const schema = {
    complain: Joi.string().allow('').allow(null),
    diagnosis: Joi.string().allow('').allow(null),
    notes: Joi.string().allow('').allow(null),
    paymentType: Joi.array().required(),
    paymentTypeValues: Joi.array().required(),
    services: Joi.array()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const reservationId = req.params.reservationId
  const body = _.pick(req.body, ['complain', 'diagnosis', 'notes', 'services', 'paymentType', 'paymentTypeValues'])

  Reservation.findById(reservationId).then(async reservation => {
    if (!reservation) {
      const error = new Error('Could not find reservation.')
      error.statusCode = 404
      throw error
    }
    reservation.complain = body.complain
    reservation.diagnosis = body.diagnosis
    reservation.notes = body.notes
    reservation.paymentType = body.paymentType ? body.paymentType : []
    reservation.paymentTypeValues = body.paymentTypeValues ? body.paymentTypeValues : []

    await ReservationService.find({ reservation: reservationId }).deleteMany()
    body.services.forEach(service => {
      let resServ = new ReservationService({
        reservation: reservationId,
        service: service.service,
        price: service.price,
        discount: service.discount
      })
      resServ.save()
    })

    return reservation.save()
  }).then(reservation => {
    res.status(200).json({
      message: 'reservation updated!',
      data: reservation
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.createBill = async (req, res, next) => {
  const reservationId = req.params.reservationId
  Reservation.findById(reservationId).then(reservation => {
    if (!reservation) {
      const error = new Error('Could not find reservation.')
      error.statusCode = 404
      throw error
    }

    Bill.find({ reservation: reservationId }).then(async bill => {
      let _bill = bill ? bill[0] : null
      if (!_bill) {
        let _paidAmount = 0
        if (reservation.paymentType[0]) {
          _paidAmount += reservation.paymentTypeValues[0]
        }
        if (reservation.paymentType[1]) {
          _paidAmount += reservation.paymentTypeValues[1]
        }

        _bill = new Bill({
          number: (await Bill.countDocuments()) + 10000,
          reservation: reservationId,
          date: new Date(),
          user: req.user._id,
          branch: req.body.branch,
          paidAmount: _paidAmount
        })
        await _bill.save()
      }
      Reservation.findOneAndUpdate({
        _id: reservationId
      }, {
        $set: {
          isDeleted: true
        }
      }, { new: true }).populate(['doctor', 'patient', 'branch']).then(async (reservation) => {
        let services = await ReservationService.find({ reservation: reservationId }).populate('service')
        res.status(200).json({
          message: 'Bill created successfully!',
          data: {
            bill: await Bill.findOne({ reservation: reservationId }).populate('user'),
            reservation,
            services
          }
        })
      })
    })
  })
}

exports.updateBill = async (req, res, next) => {
  const billId = req.params.billId
  Bill.findByIdAndUpdate(billId, { paidAmount: req.body.paidAmount }).then(() => {
    res.status(200).json({
      message: 'Bill updated!'
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getReservationsByPatient = async (req, res, next) => {
  const patientId = req.params.patientId
  Reservation.find({ patient: patientId })
    .populate(['patient', 'doctor', 'user', 'status'])
    .then(async reservations => {
      if (reservations.length == []) {
        return res.status(200).json({
          message: 'No reservation found',
          data: reservations
        })
      }
      let resvs = []
      for (let [index, reservation] of reservations.entries()) {
        console.log('reservation', reservation)
        let services = await ReservationService.find({ reservation: reservation._id }).populate(['service', {
          path: 'reservation',
          populate: ['doctor']
        }])

        resvs.push({
          reservation,
          services
        })

        if (index == reservations.length - 1) {
          res.status(200).json({
            message: 'Reservations fetched!',
            data: resvs
          })
        }
      }
    }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.gotoDoctor = async (req, res, next) => {
  const reservationId = req.params.reservationId
  const reservation = await Reservation.findById(reservationId)
  const busyDoctor = await Reservation.findOne({
    doctor: reservation.doctor,
    status: '5c8e1fd01c5c323f54e0a804',
    date: {
      $gte: moment().startOf('day').toDate(),
      $lte: moment().endOf('day').toDate()
    }
  })
  if (busyDoctor) {
    return res.status(403).json({
      type: 'warning',
      data: 'there is another patient with same doctor'
    })
  }
  await Reservation.findByIdAndUpdate(reservationId, { status: '5c8e1fd01c5c323f54e0a804' }, { new: true })
    .populate([{ path: 'patient', populate: 'contract' }, 'doctor', 'user', 'status'])
    .then(reservation => {
      res.status(200).json({
        type: 'success',
        data: reservation
      })
    })

  // let _resvsInside = await Reservation.findOne({ status: "5c8e1fd01c5c323f54e0a804", _id: { $ne: reservationId } })

  // if (_resvsInside && req.body.force != true) {
  //   res.status(200).json({
  //     type: 'exists'
  //   })
  //   return
  // }
  // else {
  //   if (req.body.force) {
  //     _resvsInside.status = "5c8e1fd41c5c323f54e0a805"
  //     _resvsInside.save()
  //   }

  //   Reservation.findOneAndUpdate({ _id: reservationId }, { $set: { status: "5c8e1fd01c5c323f54e0a804" } }).then(reservation => {
  //     res.status(200).json({
  //       type: 'success',
  //       data: reservation
  //     })
  //   }).catch((err) => {
  //     if (!err.statusCode) {
  //       err.statusCode = 500
  //     }
  //     next(err)
  //   })
  // }
}
