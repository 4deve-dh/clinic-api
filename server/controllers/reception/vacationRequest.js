const _ = require('lodash')
const VacationRequest = require('../../models/vacationRequest')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const io = require('../../socket')
const moment = require('moment')

exports.sendVacationRequest = async (req, res, next) => {
  const schema = {
    start_date: Joi.date().iso().required(),
    end_date: Joi.date().iso().min(Joi.ref('start_date')),
    note: Joi.string().allow(null).allow(''),
    status_note: Joi.string()
  }
  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['start_date', 'end_date', 'note', 'status_note'])

  let vacationInYear = await VacationRequest.countDocuments({
    user: req.user._id,
    status: 'accept',
    start_date: {
      $gte: moment().startOf('year').toDate(),
      $lt: moment().endOf('year').toDate()
    }
  })

  let vacationInMonth = await VacationRequest.countDocuments({
    user: req.user._id,
    status: 'accept',
    start_date: {
      $gte: moment(body.start_date).startOf('month').toDate(),
      $lt: moment(body.start_date).endOf('month').toDate()
    }
  })

  if (vacationInYear === req.user.branch.numberOfVacation) {
    res.status(400).json({
      type: 'year'
    })
    return
  }

  if (vacationInMonth === req.user.branch.numberOfVacationMonth) {
    res.status(400).json({
      type: 'month'
    })
    return
  }

  VacationRequest.create({
    'user': req.user._id,
    'start_date': body.start_date,
    'end_date': body.end_date,
    'branch': req.user.branch._id,
    'note': body.note ? body.note : null,
    'status_note': body.status_note ? body.status_note : null
  }).then((vacationRequest) => {
    VacationRequest.populate(vacationRequest, 'user').then((vacationRequest) => {
      io.getIO().to(req.user.branch._id).emit('vacationRequest', {
        action: 'requested',
        message: 'send vacation',
        data: vacationRequest
      })
      res.status(201).json({
        message: 'Vacation Request Created Successfully',
        data: vacationRequest
      })
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getMyVacationsRequests = (req, res) => {
  VacationRequest.find({
    user: req.userId,
    createdAt: {
      $gte: moment().startOf('month').toDate(),
      $lte: moment().endOf('month').toDate()
    }
  }).then((vacations) => {
    VacationRequest.populate(vacations, 'user').then((vacations) => {
      return res.json({
        vacations
      })
    })
  })
}
