const User = require('../../models/user')
const _ = require('lodash')
const Joi = require('joi')
const fs = require('fs')
const moment = require('moment')
const {
  validationResult
} = require('express-validator/check')
const winston = require('winston')
const sharp = require('sharp')
const path = require('path')
const request = require('request')
const settings = require('../../lib/settings')


exports.login = async (req, res, next) => {
  const schema = {
    email: Joi.string().email().required(),
    password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required()
  }
  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const body = _.pick(req.body, ['email', 'password'])
  User.findByCredentials(body.email, body.password).then((user) => {
    User.populate(user, ['role', 'branch']).then(user => {
      return user.generateAuthToken().then((token) => {
        res.header('x-auth', token).send(user)
      }).catch(err => {
        if (!err.statusCode) {
          err.statusCode = 404
        }
        next(err)
      })
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.logout = (req, res, next) => {
  User.removeToken(req.token, req.userId).then(() => {
    return res.status(200).json({
      message: 'Log out successfully'
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateProfile = async (req, res, next) => {
  const schema = {
    name: Joi.string().required(),
    address: Joi.string().required(),
    gender: Joi.string().valid('male', 'female').required(),
    birthdate: Joi.date().iso().min(1900).max(Date.now()).required(),
    mobile: Joi.string().required(),
    password: Joi.string().min(6),
    image: Joi.any()
  }
  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    const error = new Error('Validation Failed , entered data is incorrect.')
    error.statusCode = 422
    error.data = errors.array()
    throw error
  }
  let body = _.pick(req.body, ['name', 'address', 'gender', 'birthdate', 'mobile', 'password'])
  let user = await User.findById(req.userId)
  user.name = body.name
  user.address = body.address
  user.gender = body.gender
  user.birthdate = body.birthdate
  user.mobile = body.mobile
  if (body.password) {
    user.password = body.password
  }
  if (req.file) {
    fs.unlink(user.image, () => {
      console.log('deleted')
    })
    let app_url = `${settings.APP_URL}:${settings.apiListenPort}`
    filename = path.basename(req.file.path)
    let resizer = sharp().resize(600).toFile('uploads/images/users/' + filename, (err, info) => {
      console.log('err: ', err)

      console.log('info: ', info)
    })
    user.image = req.file.path
    request({url: app_url + "/" + req.file.path, encoding: null}).pipe(resizer)
  }
  user.save().then((user) => {
    res.status(200).json({
      message: 'user updated!',
      data: user
    })
  })
}

exports.forgetPassword = async (req, res, next) => {
  const schema = {
    email: Joi.string().email().required()
  }
  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    return res.status(400).json({
      type: 'error',
      message: result.error.details
    })
  }
  const body = _.pick(req.body, ['email'])
  let dt = moment()
  dt = moment(dt).add(3, 'hours')

  User.findOneAndUpdate({
    email: body.email
  }, {
    $set: {
      resetToken: Math.floor(1000 + Math.random() * 9000),
      resetExpires: dt
    }
  }, {new: true}).then((user) => {
    if (!user) {
      const error = new Error('User not found.')
      error.statusCode = 404
      throw error
    }
    User.sendEmail(user, user.resetToken)
    res.status(200).json({message: 'Reset Password Successfully'})
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.verifyToken = (req, res, next) => {
  const schema = {
    email: Joi.string().email().required(),
    code: Joi.string().required(),
    password: Joi.string().required()
  }
  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    return res.status(400).json({
      type: 'error',
      message: result.error.details
    })
  }
  const body = _.pick(req.body, ['email', 'code', 'password'])
  User.findOne({
    resetToken: body.code,
    email: body.email,
    resetExpires: {
      $gt: Date.now()
    }
  }).then((user) => {
    if (!user) {
      const error = new Error('Email not found or expired Token.')
      error.statusCode = 404
      throw error
    }
    user.password = body.password
    winston.info('password', body.password)
    user.resetExpires = null
    user.save().then(() => {
      res.status(200).json({message: 'Password changed successfully'})
    }).catch(err => {
      if (!err.statusCode) {
        err.statusCode = 404
      }
      next(err)
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 404
    }
    next(err)
  })
}
