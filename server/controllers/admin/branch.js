const _ = require('lodash')
const Branch = require('../../models/branch')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

exports.getBranches = (req, res, next) => {
  Branch.find({ isDeleted: false }).then(branches => {
    res.status(200).json({
      message: 'Fetched branches successfully.',
      data: branches
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.createBranch = async (req, res, next) => {
  const schema = {
    name: Joi.string().required(),
    phone: Joi.string().required(),
    address: Joi.string().required(),
    city: Joi.string().required(),
    profileStart: Joi.number().required(),
    numberOfVacation: Joi.number().required(),
    numberOfVacationMonth: Joi.number().required(),
    cleanVacation: Joi.number().required(),
    incomeLimit: Joi.number().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['name', 'phone', 'address', 'city', 'profileStart', 'numberOfVacation', 'numberOfVacationMonth', 'cleanVacation', 'incomeLimit'])

  let _branch = await Branch.findOne({ profileStart: body.profileStart })
  if (_branch) {
    res.status(400).json({
      type: 'error',
      message: 'Branch already exists with same profile start number!'
    })
    return
  }

  const branch = new Branch(body)

  branch.save().then((branch) => {
    res.status(201).json({
      message: 'Branch created successfully',
      data: branch
    })
  })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500
      }
      next(err)
    })
}

exports.getBranch = (req, res, next) => {
  const branchId = req.params.branchId
  Branch.findById(branchId).then(branch => {
    if (!branch) {
      const error = new Error('Could not find branch.')
      error.statusCode = 404
      throw error
    }
    res.status(200).json({
      message: 'Branch fetched.',
      data: branch
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateBranch = (req, res, next) => {
  const schema = {
    name: Joi.string().required(),
    phone: Joi.string().required(),
    address: Joi.string().required(),
    city: Joi.string().required(),
    profileStart: Joi.number().required(),
    numberOfVacation: Joi.number().required(),
    numberOfVacationMonth: Joi.number().required(),
    cleanVacation: Joi.number().required(),
    incomeLimit: Joi.number().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const branchId = req.params.branchId
  const body = _.pick(req.body, ['name', 'phone', 'address', 'city', 'profileStart', 'numberOfVacation', 'numberOfVacationMonth', 'cleanVacation', 'incomeLimit'])

  Branch.findByIdAndUpdate(branchId, body, { new: true }).then(branch => {
    res.status(200).json({
      message: 'branch updated!',
      data: branch
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.deleteBranch = async (req, res, next) => {
  const branchId = req.params.branchId
  Branch.findOneAndUpdate({
    _id: branchId,
    isDeleted: false
  }, {
    $set: {
      isDeleted: true
    }
  }, { new: true }).then((branch) => {
    if (!branch) {
      const error = new Error('Could not find branch.')
      error.statusCode = 404
      throw error
    }
    res.status(200).json({
      message: 'Deleted branch successfully.'
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.searchBranch = (req, res, next) => {
  const filter = req.query.filter
  Branch.find({
    name: new RegExp(filter, 'i')
  }).then((branch) => res.status(200).json(branch))
}
