const User = require('../../models/user')
const Role = require('../../models/roles')
const _ = require('lodash')
const Joi = require('joi')
const Attendance = require('../../models/attendance')
const fs = require('fs')
const sharp = require('sharp')
const path = require('path')
const request = require('request')

const {
  validationResult
} = require('express-validator/check')

Joi.objectId = require('joi-objectid')(Joi)

exports.createUser = (req, res, next) => {
  const schema = {
    email: Joi.string().email().required(),
    name: Joi.string().required(),
    address: Joi.string().when('role', {
      is: '5c7e54acffd81d307c66b39d', // doctor role id
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    gender: Joi.string().valid('male', 'female').when('role', {
      is: '5c7e54acffd81d307c66b39d', // doctor role id
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    birthdate: Joi.date().iso().min(1900).when('role', {
      is: '5c7e54acffd81d307c66b39d', // doctor role id
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    mobile: Joi.string().when('role', {
      is: '5c7e54acffd81d307c66b39d', // doctor role id
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    password: Joi.string().min(6),
    isAdmin: Joi.boolean().required(),
    role: Joi.objectId().when('isAdmin', {
      is: false,
      then: Joi.required(),
      otherwise: Joi.optional()
    }),
    incomeLimit: Joi.when('isAdmin', {
      is: false,
      then: Joi.number().required(),
      otherwise: Joi.optional()
    }),
    branch: Joi.objectId().required(),
    passportNumber: Joi.number().integer(),
    passportStart: Joi.date().max(Joi.ref('passportEnd')),
    passportEnd: Joi.string().when('passportStart', {
      is: Joi.date(),
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    residenceNumber: Joi.number().integer(),
    residenceStart: Joi.date().max(Joi.ref('residenceEnd')),
    residenceEnd: Joi.string().when('residenceStart', {
      is: Joi.date(),
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    workStart: Joi.date().max(Joi.ref('workEnd')),
    workEnd: Joi.string().when('workStart', {
      is: Joi.date(),
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    number: Joi.string().required(),
    salary: Joi.number()
  }
  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    const error = new Error('Validation Failed , entered data is incorrect.')
    error.statusCode = 422
    error.data = errors.array()
    throw error
  }
  let body = _.pick(req.body, ['email', 'name', 'address', 'gender', 'birthdate', 'mobile', 'role', 'password', 'isAdmin', 'branch', 'passportNumber', 'passportStart', 'passportEnd', 'residenceNumber', 'residenceStart', 'residenceEnd', 'workStart', 'workEnd', 'number', 'salary', 'incomeLimit'])

  let user = new User(body)
  if (body.isAdmin === true) {
    user.isAdmin = true
  }
  user.role = body.role
  user.branch = body.branch
  if (req.file) {
    let app_url = 'http://192.168.1.119:3000'
    filename = path.basename(req.file.path)
    let resizer = sharp().resize(600).toFile('uploads/images/users/' + filename, (err, info) => {
      console.log('err: ', err)

      console.log('info: ', info)
    })
    user.image = req.file.path
    request({url: app_url + "/" + req.file.path, encoding: null}).pipe(resizer)
  } else {
    user.image = 'uploads/images/users/default.png'
  }
  user.save().then((user) => {
    return User.populate(user, 'branch')
  }).then(async (user) => {
    let nextNumber = user.branch.profileStart
    let _users = await User.find({ isDeleted: false, branch: user.branch._id })
    for (let _user of _users) {
      if (_user.number === nextNumber) {
        nextNumber++
        continue
      }
    }
  }).then((user) => {
    res.status(200).json({ message: 'User created successfully', data: user })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateUser = async (req, res, next) => {
  const schema = {
    email: Joi.string().email().required(),
    name: Joi.string().required(),
    address: Joi.string().when('role', {
      is: '5c7e54acffd81d307c66b39d', // doctor role id
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    gender: Joi.string().valid('male', 'female').when('role', {
      is: '5c7e54acffd81d307c66b39d', // doctor role id
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    birthdate: Joi.date().iso().min(1900).when('role', {
      is: '5c7e54acffd81d307c66b39d', // doctor role id
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    mobile: Joi.string().when('role', {
      is: '5c7e54acffd81d307c66b39d', // doctor role id
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    password: Joi.string().allow(null),
    isAdmin: Joi.boolean().optional(),
    role: Joi.objectId().when('isAdmin', {
      is: false,
      then: Joi.required(),
      otherwise: Joi.optional()
    }),
    incomeLimit: Joi.when('isAdmin', {
      is: false,
      then: Joi.number().required(),
      otherwise: Joi.optional()
    }),
    image: Joi.any(),
    passportNumber: Joi.number().integer(),
    passportStart: Joi.date().max(Joi.ref('passportEnd')),
    passportEnd: Joi.string().when('passportStart', {
      is: Joi.date(),
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    residenceNumber: Joi.number().integer(),
    residenceStart: Joi.date().max(Joi.ref('residenceEnd')),
    residenceEnd: Joi.string().when('residenceStart', {
      is: Joi.date(),
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    workStart: Joi.date().max(Joi.ref('workEnd')),
    workEnd: Joi.string().when('workStart', {
      is: Joi.date(),
      then: Joi.optional(),
      otherwise: Joi.required()
    }),
    number: Joi.string(),
    salary: Joi.number()
  }
  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    const error = new Error('Validation Failed , entered data is incorrect.')
    error.statusCode = 422
    error.data = errors.array()
    throw error
  }
  const userId = req.params.userId
  let body = _.pick(req.body, ['email', 'name', 'address', 'gender', 'birthdate', 'mobile', 'password', 'isAdmin', 'role', 'passportNumber', 'passportStart', 'passportEnd', 'residenceNumber', 'residenceStart', 'residenceEnd', 'workStart', 'workEnd', 'number', 'salary', 'incomeLimit'])

  let user = await User.findOne({ _id: userId, isDeleted: false })
  if (!user) {
    const error = new Error('Could not find user may be deleted contact admin.')
    error.statusCode = 404
    throw error
  }
  user.email = body.email
  user.name = body.name
  user.address = body.address
  user.gender = body.gender
  user.birthdate = body.birthdate
  user.mobile = body.mobile
  user.number = body.number
  user.salary = body.salary

  if (body.passportNumber) user.passportNumber = body.passportNumber
  if (body.passportStart) user.passportStart = body.passportStart
  if (body.passportEnd) user.passportEnd = body.passportEnd
  if (body.residenceNumber) user.residenceNumber = body.residenceNumber
  if (body.residenceStart) user.residenceStart = body.residenceStart
  if (body.residenceEnd) user.residenceEnd = body.residenceEnd
  if (body.workStart) user.workStart = body.workStart
  if (body.workEnd) user.workEnd = body.workEnd
  if (body.password) user.password = body.password
  if (body.isAdmin !== undefined) user.isAdmin = body.isAdmin
  if (body.role) user.role = body.role
  if (body.incomeLimit) user.incomeLimit = body.incomeLimit
  console.log(req.file)
  if (req.file) {
    if (user.image != 'uploads/images/users/default.png') {
      fs.exists(user.image, function (exists) {
        if (exists) {
          fs.unlinkSync(user.image)
        }
      })
    }
    user.image = req.file.path
  }
  if (req.body.image == 'null') {
    console.log('img', user.image)
    if (user.image != 'uploads/images/users/default.png') {
      fs.unlinkSync(user.image)
    }
    user.image = 'uploads/images/users/default.png'
  }
  user.save().then((user) => {
    res.status(200).json({
      message: 'user updated!',
      data: user
    })
  })
}

exports.getUser = (req, res, next) => {
  const userId = req.params.userId
  User.findOne({ _id: userId, isDeleted: false }).then(user => {
    if (!user) {
      const error = new Error('Could not find user.')
      error.statusCode = 404
      throw error
    }
    return User.populate(user, ['branch', 'role'])
  }).then((user) => {
    res.status(200).json({
      message: 'User fetched.',
      data: user
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getAllUserBranch = async (req, res, next) => {
  const branchId = req.params.branchId
  const roleDoctor = await Role.findOne({ name: 'doctor' })
  User.find({
    _id: { $ne: req.user._id },
    branch: branchId,
    role: {
      $ne: roleDoctor._id
    },
    isDeleted: false
  }).then((users) => {
    return User.populate(users, ['role', 'branch'])
  }).then((users) => {
    res.json({
      message: 'Users fetched successfully',
      data: users
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getAllDoctorsBranch = async (req, res, next) => {
  const branchId = req.params.branchId
  const roleDoctor = await Role.findOne({ name: 'doctor' })
  User.find({
    branch: branchId,
    role: roleDoctor._id,
    isDeleted: false
  }).then((users) => {
    return User.populate(users, ['role', 'branch'])
  }).then((users) => {
    res.json({
      message: 'Users fetched successfully',
      data: users
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getAllUsersBranch = async (req, res, next) => {
  const branchId = req.params.branchId
  await User.find({
    branch: branchId,
    isDeleted: false
  }).populate(['role', 'branch']).then(async (users) => {
    let filteredUsersBranch = _.reject(users, function(val){
      return val.isAdmin !== false
    })
    res.json({
      message: 'Users fetched successfully',
      data: filteredUsersBranch
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.deleteUser = (req, res, next) => {
  const userId = req.params.userId
  User.findOneAndUpdate({
    _id: userId,
    isDeleted: false
  }, {
    $set: {
      isDeleted: true
    }
  }, { new: true }).then((user) => {
    if (!user) {
      const error = new Error('Could not find user may be deleted.')
      error.statusCode = 404
      throw error
    }
    res.status(200).json({
      message: 'User deleted successfully',
      data: user
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}
