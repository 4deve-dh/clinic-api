const _ = require('lodash')
const Contract = require('../../models/contract')
const ContractService = require('../../models/contractServices')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

exports.getContracts = (req, res, next) => {
  Contract.find({}).then(contracts => {
    res.status(200).json({
      message: 'Fetched contracts successfully.',
      data: contracts
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getContractServices = (req, res, next) => {
  const contractId = req.params.contractId
  Contract.findById(contractId).then(contract => {
    if (!contract) {
      const error = new Error('Could not find contract.')
      error.statusCode = 404
      throw error
    }

    ContractService.find({ contract: contractId }).populate('service').then(services => {
      res.status(200).json({
        message: 'Fetched contract services successfully.',
        data: services
      })
    }).catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500
      }
      next(err)
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.createContract = (req, res, next) => {
  const schema = {
    name: Joi.string().required(),
    responsible_name: Joi.string().required(),
    phone: Joi.string().required(),
    email: Joi.string().email().required(),
    address: Joi.string().required(),
    date: Joi.date().required(),
    isActive: Joi.boolean().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['name', 'responsible_name', 'phone', 'email', 'address', 'date', 'isActive'])

  const contract = new Contract({
    name: body.name,
    responsible_name: body.responsible_name,
    phone: body.phone,
    email: body.email,
    address: body.address,
    date: body.date,
    isActive: body.isActive
  })

  contract.save().then((contract) => {
    res.status(201).json({
      message: 'Contract created successfully',
      data: contract
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getContract = (req, res, next) => {
  const contractId = req.params.contractId
  Contract.findById(contractId).then(contract => {
    if (!contract) {
      const error = new Error('Could not find contract.')
      error.statusCode = 404
      throw error
    }
    res.status(200).json({
      message: 'Contract fetched.',
      data: contract
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateContract = (req, res, next) => {
  const schema = {
    name: Joi.string().required(),
    responsible_name: Joi.string().required(),
    phone: Joi.string().required(),
    email: Joi.string().email().required(),
    address: Joi.string().required(),
    date: Joi.date().required(),
    isActive: Joi.boolean().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const contractId = req.params.contractId
  const body = _.pick(req.body, ['name', 'responsible_name', 'phone', 'email', 'address', 'date', 'isActive'])

  Contract.findById(contractId).then(contract => {
    if (!contract) {
      const error = new Error('Could not find contract.')
      error.statusCode = 404
      throw error
    }
    contract.name = body.name
    contract.responsible_name = body.responsible_name
    contract.phone = body.phone
    contract.email = body.email
    contract.address = body.address
    contract.date = body.date
    contract.isActive = body.isActive

    return contract.save()
  }).then(contract => {
    res.status(200).json({
      message: 'contract updated!',
      data: contract
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.deleteContract = async (req, res, next) => {
  const contractId = req.params.contractId
  Contract.findById(contractId).then(contract => {
    if (!contract) {
      const error = new Error('Could not find contract.')
      error.statusCode = 404
      throw error
    }
    return Contract.findByIdAndRemove(contractId)
  }).then(() => {
    res.status(200).json({
      message: 'Deleted contract.'
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}
