const _ = require('lodash')
const VacationRequest = require('../../models/vacationRequest')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const io = require('../../socket')
const moment = require('moment')

exports.getAllVacationRequests = (req, res, next) => {
  VacationRequest.find({ branch: req.user.branch._id, status: 'requested' }).then(vacationRequests => {
    VacationRequest.populate(vacationRequests, 'user')
      .then(async (vacationRequests) => {
        let _data = []
        for (let vacationRequest of vacationRequests) {
          let vacationInYear = await VacationRequest.countDocuments({
            user: vacationRequest.user,
            status: 'accept',
            start_date: {
              $gte: moment().startOf('year').toDate(),
              $lt: moment().endOf('year').toDate()
            }
          })
          let vacationInMonth = await VacationRequest.countDocuments({
            user: vacationRequest.user,
            status: 'accept',
            start_date: {
              $gte: moment().startOf('month').toDate(),
              $lt: moment().endOf('month').toDate()
            }
          })

          _data.push({
            vacationRequest,
            vacationInYear,
            vacationInMonth
          })
        }
        return _data
      }).then((vacationRequests) => {
        res.status(200).json({
          message: 'Fetched vacation requests successfully.',
          data: vacationRequests
        })
      })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.acceptRejectVacations = (req, res, next) => {
  const schema = {
    status: Joi.string().valid('accept', 'reject').required(),
    status_note: Joi.string().allow(null).allow('').when('status', {
      is: 'reject',
      then: Joi.required(),
      otherwise: Joi.optional()
    })
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['status', 'status_note'])
  const vacationRequestId = req.params.vacationRequestId
  VacationRequest.findOne({ _id: vacationRequestId, status: 'requested' }).populate('branch')
    .then(async (vacationRequest) => {
      if (!vacationRequest) {
        const error = new Error('Could not find vacationRequest or already accepted.')
        error.statusCode = 404
        throw error
      }
      let vacationInYear = await VacationRequest.countDocuments({
        user: vacationRequest.user,
        status: 'accept',
        start_date: {
          $gte: moment().startOf('year').toDate(),
          $lt: moment().endOf('year').toDate()
        }
      })

      let vacationInMonth = await VacationRequest.countDocuments({
        user: vacationRequest.user,
        status: 'accept',
        start_date: {
          $gte: moment().startOf('month').toDate(),
          $lt: moment().endOf('month').toDate()
        }
      })

      if (vacationInYear > vacationRequest.branch.numberOfVacation || vacationInMonth > vacationRequest.numberOfVacationMonth) {
        return res.status(400).json({
          type: 'warning',
          message: 'you have exceed your vacation limit contact admin'
        })
      } else {
        updateStatus(vacationRequestId, body).then((vacationRequest) => {
          io.getIO().to(vacationRequest.user.branch).emit('vacationRequestResponse', {
            action: vacationRequest.status,
            message: vacationRequest.message,
            data: vacationRequest
          })
          return res.status(200).json({
            message: 'Vacation Request accepted Successfully',
            data: vacationRequest
          })
        })
      }
    }).catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500
      }
      next(err)
    })
}

const updateStatus = async (vacationRequestId, body) => {
  const vacationRequest = await VacationRequest.findByIdAndUpdate(vacationRequestId, {
    'status': body.status,
    'status_note': body.status_note ? body.status_note : null
  }, { new: true }).populate('user')
  return vacationRequest
}
