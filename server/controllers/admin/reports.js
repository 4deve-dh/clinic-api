/* eslint-disable no-return-assign */
/* eslint-disable eqeqeq */
const Reservation = require('../../models/reservation')
const Bill = require('../../models/bill')
const ExpenseData = require('../../models/expenseData')
const Patient = require('../../models/patient')
const Branch = require('../../models/branch')
const User = require('../../models/user')
const Attendance = require('../../models/attendance')
const VacationRequest = require('../../models/vacationRequest')
const ReservationService = require('../../models/reservationService')
const moment = require('moment')
const _ = require('lodash')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const mongoose = require('mongoose')

exports.dashboard = async (req, res, next) => {
  const branchId = mongoose.Types.ObjectId(req.params.branchId)

  let startOfMonth = moment().startOf('month').toDate()
  let endOfMonth = moment().endOf('month').toDate()

  let filter = {
    branch: branchId,
    date: {
      $gte: startOfMonth,
      $lte: endOfMonth
    }
  }

  let reservationsCount = await Reservation.countDocuments(filter).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })

  let expenses = await ExpenseData.aggregate([
    { $match: filter }, { $group: { _id: null, sum: { $sum: '$cost' } } }
  ]).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })

  let bills = await Bill.aggregate([
    { $match: filter }, { $group: { _id: null, sum: { $sum: '$paidAmount' } } }
  ]).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })

  res.status(200).json({
    message: 'Dashboard data fetched successfully',
    data: {
      bills: bills.length > 0 ? bills[0].sum : 0,
      reservationsCount,
      expenses: expenses.length > 0 ? expenses[0].sum : 0
    }
  })
}

exports.createReport = (req, res, next) => {
  const branchId = req.params.branchId
  const schema = {
    model: Joi.string().valid('patient', 'bill', 'expense', 'reservation', 'vacation', 'attendance'),
    type: Joi.string().valid('today', 'week', 'month', 'year', 'lifetime', 'dates'),
    patient: Joi.string(),
    user: Joi.string().allow(null),
    status: Joi.string().valid('accept', 'reject', 'all').allow(null).allow(''),
    billType: Joi.string().valid('all').allow(''),
    vacationType: Joi.string().valid('all').allow(''),
    dates: Joi.array().items(Joi.date()).allow(null),
    doctor: Joi.string().allow(''),
    nationality: Joi.string(),
    city: Joi.string(),
    paymentType: Joi.string(),
    expense: Joi.string().allow(''),
    attendance: Joi.string().allow('')
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  let modelType = req.body.model
  let type = req.body.type
  let model

  let dates
  let filter = {
    branch: branchId,
    ...(modelType == 'patient' ? { createdAt: { $gte: null, $lte: null } } : { date: { $gte: null, $lte: null } })
  }
  let populate = []
  let projection = {}

  let _key = modelType == 'patient' ? filter.createdAt : filter.date

  if (type == 'today') {
    _key.$gte = moment().startOf('day').toDate()
    _key.$lte = moment().endOf('day').toDate()
  } else if (type == 'week') {
    _key.$gte = moment().subtract(7, 'day').toDate()
    _key.$lte = moment().toDate()
  } else if (type == 'month') {
    _key.$gte = moment().startOf('month').toDate()
    _key.$lte = moment().endOf('month').toDate()
  } else if (type == 'year') {
    _key.$gte = moment().startOf('year').toDate()
    _key.$lte = moment().endOf('year').toDate()
  } else if (type == 'lifetime') {
    filter = { branch: branchId }
  } else if (type == 'dates') {
    dates = req.body.dates
    _key.$gte = dates[0]
    _key.$lte = dates[1]
  }

  if (modelType == 'bill') {
    model = Bill
    populate = [{
      path: 'reservation',
      populate: [{ path: 'patient', select: 'name_ar -_id' }, { path: 'doctor', select: 'name' }, 'branch']
    }, 'user']
    if (req.body.billType == 'all') {
      projection = {}
    } else {
      projection = { _id: 0, reservation: 1, date: 1, number: 1, paidAmount: 1 }
    }
  }
  if (modelType == 'attendance') {
    model = Attendance
    populate = [{ path: 'user' }]
    let user = req.body.user
    if (user && user !== undefined && user != 'all') {
      filter['user'] = user
    }
  }
  if (modelType == 'expense') {
    model = ExpenseData
    populate = [{ path: 'expense', select: 'name -_id' }]
    projection = { _id: 0, expense: 1, date: 1, cost: 1, description: 1 }

    let expenseType = req.body.expense
    if (expenseType && expenseType !== undefined && expenseType != 'all') {
      filter['expense'] = expenseType
    }
  }
  if (modelType == 'reservation') {
    model = Reservation
    populate = [{ path: 'patient', select: 'name_ar -_id nationality' }, {
      path: 'doctor',
      select: 'name -_id'
    }, 'status', { path: 'user', select: 'name -_id' }]
    projection = { _id: 0, patient: 1, doctor: 1, date: 1, time: 1, status: 1, paymentType: 1 }

    let doctor = req.body.doctor
    let patient = req.body.patient
    let paymentType = req.body.paymentType

    if (doctor && doctor !== undefined && doctor != 'all') {
      filter['doctor'] = doctor
    }
    if (patient && patient !== undefined && patient != 'all') {
      filter['patient'] = patient
    }
    if (paymentType && paymentType !== undefined && paymentType != 'all') {
      filter['paymentType'] = paymentType
    }
  }
  if (modelType == 'vacation') {
    model = VacationRequest
    populate = ['user', 'branch']
    if (req.body.vacationType == 'all') {
      projection = {}
    } else {
      projection = { _id: 1, status: 1, note: 1, status_note: 1, user: 1, start_date: 1, end_date: 1, branch: 1 }
    }
    let user = req.body.user
    let status = req.body.status
    if (user && user !== undefined && user != 'all') {
      filter['user'] = user
    }
    if (status && status !== undefined && status != 'all') {
      filter['status'] = status
    }
  }
  if (modelType == 'patient') {
    model = Patient
    projection = { _id: 0, number: 1, nationality: 1, name_ar: 1, mobile: 1 }

    filter.isDeleted = false

    let nationality = req.body.nationality
    let city = req.body.city

    if (nationality && nationality !== undefined && nationality != 'all') {
      filter['nationality'] = nationality
    }
    if (city && city !== undefined && city != 'all') {
      filter['address'] = city
    }
  }

  model.find(filter, projection).populate(populate).then(async data => {
    let doctor = req.body.doctor

    let sum = 0
    if (data.length > 0) {
      if (data[0].price !== undefined) {
        data.forEach(_data => sum += _data.price ? _data.price : 0)
      } else if (data[0].cost !== undefined) {
        data.forEach(_data => sum += _data.cost ? _data.cost : 0)
      } else if (data[0].paidAmount !== undefined) {
        data.forEach(_data => doctor != 'all' && doctor !== undefined ? (_data.reservation.doctor._id == doctor ? sum += _data.paidAmount : null) : sum += _data.paidAmount)
      }

      data = data.filter(d => {
        if (d.doctor === null || d.patient === null) {
          return false
        }
        if (d.user === null) {
          return false
        }
        if (d.status === null) {
          return false
        }
        if (d.reservation === null) {
          return false
        }
        if (d.expense === null) {
          return false
        }

        return true
      })
    } else {
      return res.status(200).json({
        message: 'Report created successfully.',
        data: [],
        sum: 0,
        dates: null
      })
    }
    if (modelType == 'attendance') {
      let filteredAttendenceBranch = _.reject(data, function (val) {
        return val.user.branch != branchId
      })
      return res.status(200).json({
        message: 'Report created successfully.',
        data: filteredAttendenceBranch
      })
    }

    if (modelType == 'reservation') {
      let nationality = req.body.nationality
      let city = req.body.city

      if (nationality && nationality !== undefined && nationality != 'all') {
        data = data.filter(d => d.patient.nationality == nationality)
      }
      if (city && city !== undefined && city != 'all') {
        data = data.filter(d => d.patient.address == city)
      }
    }

    if (modelType == 'bill') {
      let _data = []

      for (let [index, bill] of data.entries()) {
        let services = await ReservationService.find({ reservation: bill.reservation._id }).populate('service')

        if (doctor && doctor !== undefined && doctor != 'all') {
          if (bill.reservation.doctor._id == doctor) {
            _data.push({
              bill,
              services,
              paymentType: bill.reservation.paymentType,
              paymentTypeValues: bill.reservation.paymentTypeValues
            })
          }
        } else {
          _data.push({
            bill,
            services,
            paymentType: bill.reservation.paymentType,
            paymentTypeValues: bill.reservation.paymentTypeValues
          })
        }

        if (index == data.length - 1) {
          return res.status(200).json({
            message: 'Report created successfully.',
            data: _data,
            sum,
            dates: _key !== undefined
              ? {
                type: type,
                start: _key.$gte,
                end: _key.$lte
              } : { start: data.length > 0 ? data[0].createdAt : null }
          })
        }
      }
    } else {
      res.status(200).json({
        message: 'Report created successfully.',
        data,
        ...(data.length > 0 && (data[0].price || data[0].cost) ? { sum: sum } : {}),
        dates: _key !== undefined
          ? {
            type: type,
            start: _key.$gte,
            end: _key.$lte
          } : { start: data.length > 0 ? data[0].createdAt : null }
      })
    }
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getNotification = async (req, res, next) => {
  const branchId = req.params.branchId
  let branch = await Branch.findById(branchId)

  let _patientsReservations = []
  let _cleanUsersVacations = []
  let _userPassport = []
  let _userResidence = []
  let _userWorkEnd = []

  await Patient.find({
    branch: branchId,
    isDeleted: false,
    reservationsCount: { $gt: 0 },
    reservationsCountDate: { $ne: null }
  }).then(async patients => {
    for (let patient of patients) {
      let reservationsCount = await Reservation.countDocuments({
        status: { $ne: '5c8e1fc11c5c323f54e0a802' },
        patient: patient._id,
        date: { $gte: patient.reservationsCountDate }
      })
      let remainReservation = patient.reservationsCount - reservationsCount
      if (reservationsCount <= patient.reservationsCount) {
        _patientsReservations.push({ patient: patient, remainReservation: remainReservation })
      }
    }
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })

  await User.find({
    branch: branchId
  }).then(async users => {
    for (let user of users) {
      let passportEnd = moment(user.passportEnd).diff(moment(), 'days')
      let residenceEnd = moment(user.residenceEnd).diff(moment(), 'days')
      let userWorkEnd = moment(user.residenceEnd).diff(moment(), 'days')
      if (passportEnd <= 180) {
        if (passportEnd <= 0) {
          _userPassport.push({ type: 'expired', data: user })
        } else {
          _userPassport.push({ type: `expired In ${passportEnd}`, data: user })
        }
      }
      if (residenceEnd <= 180) {
        if (residenceEnd <= 0) {
          _userResidence.push({ type: 'expired', data: user })
        } else {
          _userResidence.push({ type: `expired In ${residenceEnd}`, data: user })
        }
      }
      if (userWorkEnd <= 120) {
        if (userWorkEnd <= 0) {
          _userWorkEnd.push({ type: 'expired', data: user })
        } else {
          _userWorkEnd.push({ type: `expired In ${userWorkEnd}`, data: user })
        }
      }
    }
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })

  await User.find({
    branch: branchId,
    createdAt: {
      $gt: moment().subtract(branch.cleanVacation, 'days').toDate()
    }
  }).then(async (users) => {
    for (let user of users) {
      let lastVacation = await VacationRequest.find({
        user: user._id,
        status: 'accept',
        start_date: {
          $lt: new Date()
        }
      }).sort({ 'end_date': -1 }).limit(1)
      if (lastVacation.length > 0) {
        let diff = moment().diff(moment(lastVacation[0].start_date), 'days')
        if (diff > branch.cleanVacation) {
          _cleanUsersVacations.push(user)
        }
      } else {
        _cleanUsersVacations.push(user)
      }
    }
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
  let userVacations = await VacationRequest.find({
    start_date: {
      $gte: moment().startOf('month').toDate(),
      $lte: moment().endOf('month').toDate()
    }
  }).populate('user')

  let allAttendance = await Attendance.find({}).populate('user')
  let filteredAttendenceBranch = _.reject(allAttendance, function (val) {
    return val.user.branch != branchId
  })
  res.status(200).json({
    _patientsReservations,
    _cleanUsersVacations,
    userVacations,
    _userPassport,
    _userResidence,
    _userWorkEnd,
    filteredAttendenceBranch
  })
}

exports.salaryReport = async (req, res, next) => {
  const branchId = req.params.branchId

  let _userSalary = []

  await User.find({
    branch: branchId
  }).then(async (users) => {
    for (let user of users) {
      await Attendance.find({
        user: user._id,
        createdAt: {
          $gte: moment(`${req.body.year}-${req.body.month}-01`).toDate(),
          $lte: moment(`${req.body.year}-${req.body.month}-01`).endOf('month').toDate()
        }
      }).then(attendance => {
        let groups = _.groupBy(attendance, function (date) {
          return moment(date.createdAt).startOf('day').format()
        }).catch((err) => {
          if (!err.statusCode) {
            err.statusCode = 500
          }
          next(err)
        })
        let hours = 0
        Object.keys(groups).forEach(key => {
          if (groups[key].length < 2) {
            delete groups[key]
          } else {
            hours += moment(_.last(groups[key]).createdAt).diff(moment(_.first(groups[key]).createdAt), 'hours')
          }
        }).catch((err) => {
          if (!err.statusCode) {
            err.statusCode = 500
          }
          next(err)
        })

        let hourSalary = user.salary / (22 * 8)
        let deduction = ((22 * 8) - hours) * hourSalary

        _userSalary.push({ user, hours, diff: (22 * 8) - hours, deduction, total: user.salary - deduction })
      })
    }
  })
  res.status(200).json({
    salary: _.map(_userSalary, function (object) {
      return _.pick(object, ['user.id', 'user.name', 'user.salary', 'hours', 'diff', 'deduction', 'total'])
    })
  })
}

exports.getFilteredAttendance = async (req, res) => {
  const schema = {
    type: Joi.string().valid('today', 'week', 'month', 'year', 'all', 'dates'),
    user: Joi.string().allow(null),
    branch: Joi.objectId().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  let filter = { createdAt: { $gte: null, $lte: null } }
  let type = req.body.type
  let branch = req.body.branch
  let user = req.body.user

  let _key = filter.createdAt

  if (type == 'today') {
    _key.$gte = moment().startOf('day').toDate()
    _key.$lte = moment().endOf('day').toDate()
  } else if (type == 'week') {
    _key.$gte = moment().subtract(7, 'day').toDate()
    _key.$lte = moment().toDate()
  } else if (type == 'all') {
    filter = {}
  }
  if (user && user !== undefined && user != 'all') {
    filter['user'] = user
  }

  let allAttendance = await Attendance.find(filter).populate('user')
  let filteredAttendenceBranch = _.reject(allAttendance, function (val) {
    return val.user.branch != branch
  })
  res.status(200).json({
    filteredAttendenceBranch
  })
}
