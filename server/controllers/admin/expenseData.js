const _ = require('lodash')
const ExpenseData = require('../../models/expenseData')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

exports.getExpensesData = (req, res, next) => {
  const branchId = req.params.branchId
  ExpenseData.find({ branch: branchId }).then(expensesData => {
    return ExpenseData.populate(expensesData, ['expense', 'branch'])
  }).then((expensesData) => {
    res.status(200).json({
      message: 'Fetched expenses data successfully.',
      data: expensesData
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.createExpenseData = (req, res, next) => {
  const schema = {
    date: Joi.date().required(),
    expense: Joi.objectId().required(),
    cost: Joi.number().required(),
    description: Joi.string().allow(null).allow(''),
    branch: Joi.objectId().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['date', 'expense', 'cost', 'description', 'branch'])

  const expenseData = new ExpenseData({
    date: body.date,
    expense: body.expense,
    cost: body.cost,
    description: body.description,
    branch: body.branch
  })

  expenseData.save().then((expenseData) => {
    ExpenseData.populate(expenseData, ['expense', 'branch']).then(expenseData => {
      res.status(201).json({
        message: 'Expense Data created successfully',
        data: expenseData
      })
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getExpenseData = (req, res, next) => {
  const expenseDataId = req.params.expenseDataId
  ExpenseData.findById(expenseDataId).then(expenseData => {
    if (!expenseData) {
      const error = new Error('Could not find expenseData.')
      error.statusCode = 404
      throw error
    }
    return ExpenseData.populate(expenseData, ['expense', 'branch'])
  }).then((expenseData) => {
    res.status(200).json({
      message: 'Expense Data fetched.',
      data: expenseData
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateExpenseData = (req, res, next) => {
  const schema = {
    date: Joi.date().required(),
    expense: Joi.objectId().required(),
    cost: Joi.number().required(),
    description: Joi.string().allow(null).allow(''),
    branch: Joi.objectId().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const expenseDataId = req.params.expenseDataId
  const body = _.pick(req.body, ['date', 'expense', 'cost', 'description', 'branch'])

  ExpenseData.findById(expenseDataId).then(expenseData => {
    if (!expenseData) {
      const error = new Error('Could not find expenseData.')
      error.statusCode = 404
      throw error
    }
    expenseData.date = body.date
    expenseData.expense = body.expense
    expenseData.cost = body.cost
    expenseData.description = body.description
    expenseData.branch = body.branch
    return expenseData.save()
  }).then(expenseData => {
    ExpenseData.populate(expenseData, ['expense', 'branch']).then(expenseData => {
      res.status(200).json({
        message: 'expense Data updated!',
        data: expenseData
      })
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.deleteExpenseData = async (req, res, next) => {
  const expenseDataId = req.params.expenseDataId
  ExpenseData.findById(expenseDataId).then(expenseData => {
    if (!expenseData) {
      const error = new Error('Could not find expense data.')
      error.statusCode = 404
      throw error
    }
    return ExpenseData.findByIdAndRemove(expenseDataId)
  }).then(() => {
    res.status(200).json({
      message: 'Deleted expense Data.'
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}
