const _ = require('lodash')
const Role = require('../../models/roles')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

exports.getRoles = (req, res, next) => {
  Role.find({}).then(roles => {
    res.status(200).json({
      message: 'Fetched roles successfully.',
      data: roles
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.createRole = (req, res, next) => {
  const schema = {
    name: Joi.string().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['name'])

  const role = new Role({
    name: body.name
  })

  role.save().then((role) => {
    res.status(201).json({
      message: 'Role created successfully',
      data: role
    })
  })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500
      }
      next(err)
    })
}

exports.getRole = (req, res, next) => {
  const roleId = req.params.roleId
  Role.findById(roleId).then(role => {
    if (!role) {
      const error = new Error('Could not find role.')
      error.statusCode = 404
      throw error
    }
  }).then((role) => {
    res.status(200).json({
      message: 'Role fetched.',
      data: role
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateRole = (req, res, next) => {
  const schema = {
    name: Joi.string().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const roleId = req.params.roleId
  const body = _.pick(req.body, ['name'])

  Role.findById(roleId).then(role => {
    if (!role) {
      const error = new Error('Could not find role.')
      error.statusCode = 404
      throw error
    }
    role.name = body.title
    role.description = body.description
    return role.save()
  }).then(role => {
    return role.populate(role, {
      path: 'creator',
      select: '_id email name'
    })
  }).then((role) => {
    res.status(200).json({
      message: 'role updated!',
      data: role
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.deleteRole = async (req, res, next) => {
  const roleId = req.params.roleId
  Role.findById(roleId).then(role => {
    if (!role) {
      const error = new Error('Could not find role.')
      error.statusCode = 404
      throw error
    }
    return Role.findByIdAndRemove(roleId)
  }).then(() => {
    res.status(200).json({
      message: 'Deleted role.'
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.searchRole = (req, res) => {
  const filter = req.query.filter
  Role.find({
    name: new RegExp(filter, 'i')
  }).then((role) => res.status(200).json(role))
}
