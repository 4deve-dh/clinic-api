const _ = require('lodash')
const Patient = require('../../models/patient')
const Branch = require('../../models/branch')
const Bill = require('../../models/bill')
const Reservation = require('../../models/reservation')
const ReservationService = require('../../models/reservationService')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

exports.createpatient = async (req, res, next) => {
  const schema = {
    number: Joi.string().required(),
    name_ar: Joi.string().required(),
    name_en: Joi.string().required(),
    age: Joi.date().required(),
    address: Joi.string().required(),
    gender: Joi.string().valid('male', 'female').required(),
    mobile: Joi.number().required(),
    notes: Joi.string().allow(''),
    nationality: Joi.string().required(),
    branch: Joi.objectId().required(),
    type: Joi.boolean().allow(null),
    reservationsCount: Joi.number().allow(null).allow('').when('type', {
      is: true,
      then: Joi.required(),
      otherwise: Joi.optional()
    })
    // contract: Joi.objectId().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  let body = _.pick(req.body, ['name_ar', 'name_en', 'age', 'gender', 'mobile', 'address', 'notes', 'number', 'nationality', 'branch', 'reservationsCount'])
  let branch = await Branch.findById(req.user.branch._id)

  if (body.type == null) {
    body.type = false
  }

  if (body.reservationsCount == null) {
    body.reservationsCount = 0
  }

  if (!branch) {
    const error = new Error('Could not find branch.')
    error.statusCode = 404
    throw error
  }

  let _patient = await Patient.findOne({ number: body.number, branch: branch._id, isDeleted: false })
  if (_patient) {
    res.status(400).json({
      type: 'error',
      message: 'Patient already exists!'
    })
    return
  }

  let patient = new Patient(body)

  patient.save().then((patient) => {
    return Patient.populate(patient, [])// 'contract'
  }).then(async (patient) => {
    let nextNumber = branch.profileStart
    let _patients = await Patient.find({ isDeleted: false, branch: branch._id }).sort({ number: 1 })
    for (let _patient of _patients) {
      if (_patient.number == nextNumber) {
        nextNumber++
        continue
      }
    }
    if (body.reservationsCount) {
      const _patient = await Patient.findByIdAndUpdate(patient._id, { reservationsCountDate: Date.now() }, { new: true })
      return res.status(201).json({
        message: 'Patient created successfully',
        data: _patient,
        nextNumber
      })
    }
    res.status(201).json({
      message: 'Patient created successfully',
      data: patient,
      nextNumber
    })
  })
}

exports.updatepatient = async (req, res, next) => {
  const schema = {
    number: Joi.string().required(),
    name_ar: Joi.string().required(),
    name_en: Joi.string().required(),
    age: Joi.date().required(),
    address: Joi.string().required(),
    gender: Joi.string().valid('male', 'female').required(),
    mobile: Joi.number().required(),
    notes: Joi.string().allow(''),
    nationality: Joi.string().required(),
    reservationsCount: Joi.number().allow(null),
    type: Joi.boolean()
    // contract: Joi.objectId().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const patientId = req.params.patientId
  const body = _.pick(req.body, ['name_ar', 'name_en', 'gender', 'mobile', 'age', 'address', 'notes', 'number', 'nationality', 'reservationsCount', 'type'])
  Patient.patientExist(patientId).then((patient) => {
    if (!patient) {
      const error = new Error('Could not find patient.')
      error.statusCode = 404
      throw error
    }
    return Patient.updateOne({ _id: patientId }, body).then(() => {
      return Patient.findById(patient._id)
    }).then(async (patient) => {
      let branch = await Branch.findById(req.user.branch._id)
      let nextNumber = branch.profileStart
      let _patients = await Patient.find({ isDeleted: false, branch: branch._id }).sort({ number: 1 })
      for (let _patient of _patients) {
        if (_patient.number == nextNumber) {
          nextNumber++
          continue
        }
      }

      if (body.reservationsCount && body.type) {
        const _patient = await Patient.findByIdAndUpdate(patient._id, {
          reservationsCount: body.reservationsCount,
          reservationsCountDate: Date.now()
        }, { new: true })
        return res.status(200).json({
          message: 'Patient updated successfully!',
          data: _patient,
          nextNumber
        })
      }
      const finalPatient = await Patient.findByIdAndUpdate(patient._id, { reservationsCount: body.reservationsCount }, { new: true })

      res.status(200).json({
        message: 'Patient updated successfully!',
        data: finalPatient,
        nextNumber
      })
    })
  })
}

exports.deletePatient = (req, res, next) => {
  const patientId = req.params.patientId
  Patient.findOneAndUpdate({
    _id: patientId,
    isDeleted: false
  }, {
    $set: {
      isDeleted: true
    }
  }, { new: true }).then(async (patient) => {
    if (!patient) {
      const error = new Error('Could not find patient may be deleted.')
      error.statusCode = 404
      throw error
    }

    let branch = await Branch.findById(req.user.branch._id)
    let nextNumber = branch.profileStart
    let _patients = await Patient.find({ isDeleted: false, branch: branch._id }).sort({ number: 1 })
    for (let _patient of _patients) {
      if (_patient.number == nextNumber) {
        nextNumber++
        continue
      }
    }
    // let _patient = await Patient.findOne({ number: nextNumber })
    // if (_patient) {
    //   _patient = await Patient.find({ isDeleted: false, branch: branch._id }).sort({ number: -1 })
    //   nextNumber = parseInt(_patient[0].number) + 1
    // }

    res.status(200).json({
      message: 'Patient deleted successfully',
      data: patient,
      nextNumber
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getPatients = async (req, res, next) => {
  const currentPage = parseInt(req.query.page) || 1
  const perPage = 10
  const branchId = req.params.branchId

  let totalCount = await Patient.find({
    isDeleted: false,
    branch: branchId
  }).countDocuments()

  Patient.find({
    isDeleted: false,
    branch: branchId
  }).sort({ createdAt: -1 }).skip((currentPage - 1) * perPage).limit(perPage).then((patients) => {
    return Patient.populate(patients, [])// 'contract'
  }).then(async (patients) => {
    let branch = await Branch.findById(branchId)

    let nextNumber = branch.profileStart

    let _patients = await Patient.find({ isDeleted: false, branch: branchId }).sort({ number: 1 })
    for (let _patient of _patients) {
      if (_patient.number == nextNumber) {
        nextNumber++
        continue
      }
    }
    // let _patient = await Patient.findOne({ number: nextNumber })
    // if (_patient) {
    //   _patient = await Patient.find({ isDeleted: false, branch: branchId }).sort({ number: -1 })
    //   nextNumber = parseInt(_patient[0].number) + 1
    // }

    return res.json({
      message: 'patients fetched successfully',
      data: patients,
      meta: {
        total: totalCount,
        pages: Math.ceil(totalCount / perPage),
        perPage,
        currentPage
      },
      nextNumber
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getPatient = (req, res, next) => {
  const patientId = req.params.patientId
  Patient.findById(patientId).then(patient => {
    if (!patient || patient.isDeleted) {
      const error = new Error('Could not find patient.')
      error.statusCode = 404
      throw error
    }
    res.status(200).json({
      message: 'Patient fetched.',
      data: patient
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getPatientBills = (req, res, next) => {
  const patientId = req.params.patientId

  Reservation.find({ patient: patientId }).then(async reservations => {
    let bills = await Bill.find({ reservation: { $in: reservations.map(r => r._id) } }).populate([{
      path: 'reservation',
      populate: ['patient', 'doctor']
    }, 'branch', 'user'])

    let data = []
    for (let [index, bill] of bills.entries()) {
      let services = await ReservationService.find({ reservation: bill.reservation._id }).populate(['service', {
        path: 'reservation',
        populate: ['doctor']
      }])
      data.push({
        bill: bill,
        services
      })

      if (index == bills.length - 1) {
        return res.status(200).json({
          message: 'Bills fetched.',
          data: data
        })
      }
    }

    if (bills.length == 0) {
      res.status(200).json({
        message: 'Bills fetched.',
        data: data
      })
    }
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.searchpatient = (req, res, next) => {
  const branchId = req.params.branchId

  const schema = {
    type: Joi.string().required(),
    filter: Joi.string().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const filter = req.body.filter
  let body = _.pick(req.body, ['type'])

  if (body.type == 'mobile') {
    res.status(200).json({ type: 'success', exists: false })
    return
  }

  let _filter = { branch: branchId, isDeleted: false }
  if (body.type == 'mobile') {
    _filter.mobile = filter.trim()
  } else {
    _filter.number = filter.trim()
  }

  Patient.find(_filter).then((data) => {
    res.status(200).json({ type: 'success', exists: data.length > 0 })
  }).catch(() => res.status(200).json({ type: 'success', exists: false }))
}
