const _ = require('lodash')
const ContractService = require('../../models/contractServices')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

exports.getContractsServices = (req, res, next) => {
  ContractService.find({}).then(contractServices => {
    return ContractService.populate(contractServices, ['contract', 'service'])
  }).then((contractServices) => {
    res.status(200).json({
      message: 'Fetched contract services successfully.',
      data: contractServices
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.createContractService = (req, res, next) => {
  const schema = {
    contract: Joi.objectId().required(),
    service: Joi.objectId().required(),
    price: Joi.string().required(),
    startDate: Joi.date().required(),
    endDate: Joi.date().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['contract', 'service', 'price', 'startDate', 'endDate'])

  const contractService = new ContractService({
    contract: body.contract,
    service: body.service,
    price: body.price,
    startDate: body.startDate,
    endDate: body.endDate
  })

  contractService.save().then((contractService) => {
    ContractService.populate(contractService, ['contract', 'service']).then((contractService) => {
      res.status(201).json({
        message: 'Contract Service created successfully',
        data: contractService
      })
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getContractService = (req, res, next) => {
  const contractServiceId = req.params.contractServiceId
  ContractService.findById(contractServiceId).then(contractService => {
    if (!contractService) {
      const error = new Error('Could not find contract service.')
      error.statusCode = 404
      throw error
    }
    return ContractService.populate(contractService, ['contract', 'service'])
  }).then((contractService) => {
    res.status(200).json({
      message: 'contract service fetched.',
      data: contractService
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateContractService = (req, res, next) => {
  const schema = {
    contract: Joi.objectId().required(),
    service: Joi.objectId().required(),
    price: Joi.string().required(),
    startDate: Joi.date().required(),
    endDate: Joi.date().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const contractServiceId = req.params.contractServiceId
  const body = _.pick(req.body, ['contract', 'service', 'price', 'startDate', 'endDate'])

  ContractService.findById(contractServiceId).then(contractService => {
    if (!contractService) {
      const error = new Error('Could not find contract service.')
      error.statusCode = 404
      throw error
    }
    contractService.contract = body.contract
    contractService.service = body.service
    contractService.price = body.price
    contractService.startDate = body.startDate
    contractService.endDate = body.endDate
    contractService.save()
    return ContractService.populate(contractService, ['contract', 'service'])
  }).then(contractService => {
    res.status(200).json({
      message: 'contract service updated!',
      data: contractService
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.deleteContractService = async (req, res, next) => {
  const contractServiceId = req.params.contractServiceId
  ContractService.findById(contractServiceId).then(contractService => {
    if (!contractService) {
      const error = new Error('Could not find contract service.')
      error.statusCode = 404
      throw error
    }
    return ContractService.findByIdAndRemove(contractServiceId)
  }).then(() => {
    res.status(200).json({
      message: 'Deleted contract service.'
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}
