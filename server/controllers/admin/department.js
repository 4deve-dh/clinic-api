const _ = require('lodash')
const Department = require('../../models/department')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

exports.getDepartments = (req, res, next) => {
  Department.find({}).then(departments => {
    res.status(200).json({
      message: 'Fetched departments successfully.',
      data: departments
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.createDepartment = (req, res, next) => {
  const schema = {
    name: Joi.string().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['name'])

  const department = new Department({
    name: body.name
  })

  department.save().then((department) => {
    res.status(201).json({
      message: 'Department created successfully',
      data: department
    })
  })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500
      }
      next(err)
    })
}

exports.getDepartment = (req, res, next) => {
  const departmentId = req.params.departmentId
  Department.findById(departmentId).then((department) => {
    if (!department) {
      const error = new Error('Could not find department.')
      error.statusCode = 404
      throw error
    }
    res.status(200).json({
      message: 'Department fetched.',
      data: department
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateDepartment = (req, res, next) => {
  const schema = {
    name: Joi.string().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const departmentId = req.params.departmentId
  const body = _.pick(req.body, ['name'])

  Department.findById(departmentId).then(department => {
    if (!department) {
      const error = new Error('Could not find department.')
      error.statusCode = 404
      throw error
    }
    department.name = body.name
    return department.save()
  }).then(department => {
    res.status(200).json({
      message: 'department updated!',
      data: department
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.deleteDepartment = async (req, res, next) => {
  const departmentId = req.params.departmentId
  Department.findById(departmentId).then(department => {
    if (!department) {
      const error = new Error('Could not find department.')
      error.statusCode = 404
      throw error
    }
    return Department.findByIdAndRemove(departmentId)
  }).then(() => {
    res.status(200).json({
      message: 'Deleted department.'
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.searchDepartment = (req, res, next) => {
  const filter = req.query.filter
  Department.find({
    name: new RegExp(filter, 'i')
  }).then((department) => res.status(200).json(department))
}
