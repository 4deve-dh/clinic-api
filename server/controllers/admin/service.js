const _ = require('lodash')
const Service = require('../../models/service')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const path = require('path')
const fs = require('fs')

exports.getServices = (req, res, next) => {
  const branchId = req.params.branchId
  Service.find({ isDeleted: false, branch: branchId }).then(services => {
    Service.populate(services, 'related').then((services) => {
      res.status(200).json({
        message: 'Fetched services successfully.',
        data: services
      })
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.createService = async (req, res, next) => {
  const schema = {
    name: Joi.string().required(),
    price: Joi.number().required(),
    related: Joi.string().required(),
    videoFile: Joi.string()
  }
  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['name', 'price', 'related'])
  body.related = JSON.parse(body.related)
  // const branchId = req.params.branchId

  const service = new Service({
    name: body.name,
    price: body.price,
    branch: req.user.branch._id,
    videoUrl: req.file ? req.file.path : null
  })
  service.save().then(async (service) => {
    await Service.findByIdAndUpdate(service._id,
      { $set: { related: body.related } }, { new: true })
      .populate('related')
      .then((service) => {
        return res.status(201).json({
          message: 'Service created successfully',
          data: service
        })
      })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getService = (req, res, next) => {
  const serviceId = req.params.serviceId
  Service.findById(serviceId).then(service => {
    if (!service) {
      const error = new Error('Could not find service.')
      error.statusCode = 404
      throw error
    }
    Service.populate(service, 'related').then((service) => {
      res.status(200).json({
        message: 'Service fetched.',
        data: service
      })
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateService = (req, res, next) => {
  const schema = {
    name: Joi.string().required(),
    price: Joi.number().required(),
    related: Joi.string().required(),
    delete: Joi.boolean(),
    videoFile: Joi.string()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const serviceId = req.params.serviceId
  const body = _.pick(req.body, ['name', 'price', 'related', 'videoUrl', 'delete'])
  body.related = JSON.parse(body.related)
  Service.findById(serviceId).then(service => {
    if (!service) {
      const error = new Error('Could not find service.')
      error.statusCode = 404
      throw error
    }
    if (body.delete) {
      clearImage(service.videoUrl)
      service.videoUrl = null
    }
    service.name = body.name
    service.price = body.price
    if (req.file) {
      clearImage(service.videoUrl)
      service.videoUrl = req.file ? req.file.path : null
    }
    return service.save()
  }).then(async service => {
    await Service.findByIdAndUpdate(service._id, { $set: { related: body.related } }, { new: true })
      .populate('related')
      .then((service) => {
        return res.status(201).json({
          message: 'Service updated successfully',
          data: service
        })
      })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.deleteService = async (req, res, next) => {
  const serviceId = req.params.serviceId
  Service.findOneAndUpdate({
    _id: serviceId,
    isDeleted: false
  }, {
    $set: {
      isDeleted: true
    }
  }, { new: true }).then((service) => {
    if (!service) {
      const error = new Error('Could not find service.')
      error.statusCode = 404
      throw error
    }
    res.status(200).json({
      message: 'Deleted service.'
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.searchService = (req, res, next) => {
  const filter = req.query.filter
  Service.find({
    name: new RegExp(filter, 'i')
  }).then((service) => res.status(200).json(service))
}

const clearImage = async filePath => {
  filePath = path.join(__dirname, '..', '..', '..', filePath)
  console.log((filePath))
  await fs.unlink(filePath, (err) => {
    console.log(err)
    console.log('File deleted!')
  })
}
