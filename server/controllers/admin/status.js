const _ = require('lodash')
const Status = require('../../models/status')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

exports.getstatuses = (req, res, next) => {
  Status.find({}).then(statuses => {
    res.status(200).json({
      message: 'Fetched statuses successfully.',
      data: statuses
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.createStatus = (req, res, next) => {
  const schema = {
    name: Joi.string().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['name'])

  const status = new Status({
    name: body.name
  })

  status.save().then((status) => {
    res.status(201).json({
      message: 'status created successfully',
      data: status
    })
  })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500
      }
      next(err)
    })
}

exports.getStatus = (req, res, next) => {
  const statusId = req.params.statusId
  Status.findById(statusId).then(status => {
    if (!status) {
      const error = new Error('Could not find status.')
      error.statusCode = 404
      throw error
    }
    res.status(200).json({
      message: 'Status fetched.',
      data: status
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateStatus = (req, res, next) => {
  const schema = {
    name: Joi.string().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const statusId = req.params.statusId
  const body = _.pick(req.body, ['name'])

  Status.findById(statusId).then(status => {
    if (!status) {
      const error = new Error('Could not find status.')
      error.statusCode = 404
      throw error
    }
    status.name = body.name
    return status.save()
  }).then(status => {
    res.status(200).json({
      message: 'status updated!',
      data: status
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.deleteStatus = async (req, res, next) => {
  const statusId = req.params.statusId
  Status.findById(statusId).then(status => {
    if (!status) {
      const error = new Error('Could not find status.')
      error.statusCode = 404
      throw error
    }
    return Status.findByIdAndRemove(statusId)
  }).then(() => {
    res.status(200).json({
      message: 'Deleted status.'
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.searchStatus = (req, res, next) => {
  const filter = req.query.filter
  Status.find({
    name: new RegExp(filter, 'i')
  }).then((status) => res.status(200).json(status))
}
