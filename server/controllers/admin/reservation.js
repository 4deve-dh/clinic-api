const Reservation = require('../../models/reservation')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

exports.getReservations = (req, res, next) => {
  Reservation.find({}).then(reservations => {
    return Reservation.populate(reservations, ['patient', 'doctor', 'user'])
  }).then((reservations) => {
    res.status(200).json({
      message: 'Fetched reservations successfully.',
      data: reservations
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}
