const _ = require('lodash')
const Schedule = require('../../models/schedule')
const User = require('../../models/user')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

exports.createSchedule = (req, res, next) => {
  const schema = {
    doctor: Joi.objectId().required(),
    day_of_week: Joi.number().required(),
    start_time: Joi.string().required(),
    end_time: Joi.string().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['doctor', 'day_of_week', 'start_time', 'end_time'])
  User.findById(body.doctor).populate(['branch']).then((doctor) => {
    const schedule = new Schedule({
      doctor: doctor._id,
      branch: doctor.branch._id,
      day_of_week: body.day_of_week,
      start_time: body.start_time,
      end_time: body.end_time
    })
    schedule.save().then((schedule) => {
      return Schedule.populate(schedule, ['doctor', 'branch'])
    }).then((schedule) => {
      res.status(201).json({
        message: 'Schedule created successfully',
        data: schedule
      })
    }).catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 404
        err.message = 'Not found schedule'
      }
      next(err)
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getSchedules = (req, res, next) => {
  const doctorId = req.params.doctorId
  Schedule.find({ doctor: doctorId, branch: req.query.branch }).then(schedules => {
    if (!schedules) {
      schedules = []
    }
    return Schedule.populate(schedules, ['branch', 'doctor'])
  }).then((schedule) => {
    res.status(200).json({
      message: 'Schedule fetched.',
      data: schedule
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateSchedule = (req, res, next) => {
  const schema = {
    doctor: Joi.objectId().required(),
    day_of_week: Joi.number().required(),
    start_time: Joi.string().required(),
    end_time: Joi.string().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const scheduleId = req.params.scheduleId
  const body = _.pick(req.body, ['doctor', 'day_of_week', 'start_time', 'end_time'])

  Schedule.findById(scheduleId).then(schedule => {
    if (!schedule) {
      const error = new Error('Could not find schedule.')
      error.statusCode = 404
      throw error
    }
    schedule.doctor = body.doctor
    schedule.day_of_week = body.day_of_week
    schedule.start_time = body.start_time
    schedule.end_time = body.end_time
    return schedule.save()
  }).then(schedule => {
    res.status(200).json({
      message: 'schedule updated!',
      data: schedule
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.deleteSchedule = async (req, res, next) => {
  const scheduleId = req.params.scheduleId
  Schedule.findById(scheduleId).then(schedule => {
    if (!schedule) {
      const error = new Error('Could not find schedule.')
      error.statusCode = 404
      throw error
    }
    return Schedule.findByIdAndRemove(scheduleId)
  }).then(() => {
    res.status(200).json({
      message: 'Deleted schedule.'
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}
