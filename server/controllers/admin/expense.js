const _ = require('lodash')
const Expense = require('../../models/expense')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

exports.getExpenses = (req, res, next) => {
  const branchId = req.params.branchId
  Expense.find({ branch: branchId }).then(expenses => {
    res.status(200).json({
      message: 'Fetched expenses successfully.',
      data: expenses
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.createExpense = (req, res, next) => {
  const schema = {
    name: Joi.string().required(),
    isActive: Joi.boolean().required().allow(null)
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['name', 'isActive'])
  const branchId = req.params.branchId

  if (body.isActive === null) {
    body.isActive = false
  }

  const expense = new Expense({
    name: body.name,
    isActive: body.isActive,
    branch: branchId
  })

  expense.save().then((expense) => {
    res.status(201).json({
      message: 'Expense created successfully',
      data: expense
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.getExpense = (req, res, next) => {
  const expenseId = req.params.expenseId
  Expense.findById(expenseId).then(expense => {
    if (!expense) {
      const error = new Error('Could not find expense.')
      error.statusCode = 404
      throw error
    }
    res.status(200).json({
      message: 'Expense fetched.',
      data: expense
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.updateExpense = (req, res, next) => {
  const schema = {
    name: Joi.string().required(),
    isActive: Joi.boolean().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }

  const expenseId = req.params.expenseId
  const body = _.pick(req.body, ['name', 'isActive'])

  Expense.findById(expenseId).then(expense => {
    if (!expense) {
      const error = new Error('Could not find expense.')
      error.statusCode = 404
      throw error
    }
    expense.name = body.name
    expense.isActive = body.isActive
    return expense.save()
  }).then(expense => {
    res.status(200).json({
      message: 'expense updated!',
      data: expense
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.deleteExpense = async (req, res, next) => {
  const expenseId = req.params.expenseId
  Expense.findById(expenseId).then(expense => {
    if (!expense) {
      const error = new Error('Could not find expense.')
      error.statusCode = 404
      throw error
    }
    return Expense.findByIdAndRemove(expenseId)
  }).then(() => {
    res.status(200).json({
      message: 'Deleted expense.'
    })
  }).catch((err) => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}

exports.searchExpense = (req, res, next) => {
  const filter = req.query.filter
  Expense.find({
    name: new RegExp(filter, 'i')
  }).then((expense) => res.status(200).json(expense))
}
