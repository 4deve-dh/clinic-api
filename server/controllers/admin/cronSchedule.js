const Bill = require('../../models/bill')
const User = require('../../models/user')
const Branch = require('../../models/branch')
const _ = require('lodash')
const moment = require('moment')
const io = require('../../socket')
const Nexmo = require('nexmo')
const nexmo = new Nexmo({
  apiKey: '7d37ab2d',
  apiSecret: 'R4ytxS602xG0FiEO'
})

exports.getAI = async () => {
  console.log('Ai started')
  let startOfMonth = moment().startOf('month').toDate()
  let endOfMonth = moment().endOf('month').toDate()
  let billsByDoctors = []
  const from = 'Nexmo'
  const opts = { type: 'unicode' }

  const bills = await Bill.find({
    date: {
      $gte: startOfMonth,
      $lte: endOfMonth
    }
  })

  let billsByBranch = _(bills).groupBy('branch').map((obj, key) => ({
    'branch': key,
    'obj': obj,
    'income': _.sumBy(obj, 'paidAmount')
  })).values()

  for (let _branch of billsByBranch) {
    let branch = await Branch.findById(_branch.branch)
    if (_branch.income < branch.incomeLimit) {
      await User.find({ isAdmin: true, branch: _branch.branch }).then(async (users) => {

        for (let user of users) {
          let text = branch.name + 'not reached target yet'
          io.getIO().to(user.branch).emit('getAI', {
            data: text
          })
          await nexmo.message.sendSms(from, user.mobile, text, opts, (err, responseData) => {
            if (err) {
              console.log(err)
            } else {
              if (responseData.messages[0]['status'] === '0') {
                console.log('Message sent successfully.')
              } else {
                console.log(`Message failed with error: ${responseData.messages[0]['error-text']}`)
              }
            }
          })
        }
      })

      await User.find({ role: '5c7e54acffd81d307c66b39d' }).then(async (users) => {
        for (let user of users) {
          await Bill.find({
            'user': user._id,
            date: {
              $gte: startOfMonth,
              $lte: endOfMonth
            }
          }).populate('user').then(async (bills) => {
            let results = await _(bills).groupBy('user._id').map((objs, key) => {
              return {
                'user': objs.find(o => o.user._id == key).user,
                'income': _.sumBy(objs, 'paidAmount')
              }
            }).value()
            for (let result of results) {
              if (result && result.income < result.user.incomeLimit) {
                billsByDoctors.push(result)
              }
            }
          })
        }
      })
    }
  }
  let doctors = _.uniqBy(billsByDoctors, 'branch')
  for (let doctor of doctors) {
    const branch = await Branch.findOne({ _id: doctor.user.branch })
    const users = await User.find({ isAdmin: true, branch: branch._id })
    for (let user of users) {
      io.getIO().to(user.branch).emit('getAI', {
        data: `you have ${billsByDoctors.length} doctors not reached their limit`
      })
      await nexmo.message.sendSms(from, user.mobile, `you have ${billsByDoctors.length} doctors not reached their limit`, opts, (err, responseData) => {
        if (err) {
          console.log(err)
        } else {
          if (responseData.messages[0]['status'] === '0') {
            console.log('Message sent successfully.')
          } else {
            console.log(`Message failed with error: ${responseData.messages[0]['error-text']}`)
          }
        }
      })
    }
  }
  return {
    doctorNumbers: billsByDoctors.length,
    billsByBranch,
    billsByDoctors
  }
}

exports.details = async (req, res) => {
  const branchId = req.params.branchId
  let startOfMonth = moment().startOf('month').toDate()
  let endOfMonth = moment().endOf('month').toDate()
  let billsByDoctors = []

  const bills = await Bill.find({
    branch: branchId,
    date: {
      $gte: startOfMonth,
      $lte: endOfMonth
    }
  })

  let billsByBranch = _(bills).groupBy('branch').map((obj, key) => ({
    'branch': key,
    'obj': obj,
    'income': _.sumBy(obj, 'paidAmount')
  })).values()

  for (let _branch of billsByBranch) {
    let branch = await Branch.findById(_branch.branch)
    if (_branch.income < branch.incomeLimit) {
      await User.find({ role: '5c7e54acffd81d307c66b39d' }).then(async (users) => {
        for (let user of users) {
          await Bill.find({
            'user': user._id,
            date: {
              $gte: startOfMonth,
              $lte: endOfMonth
            }
          }).populate('user').then(async (bills) => {
            let results = await _(bills).groupBy('user._id').map((objs, key) => {
              return {
                'user': objs.find(o => o.user._id == key).user,
                'income': _.sumBy(objs, 'paidAmount')
              }
            }).value()
            for (let result of results) {
              if (result && result.income < result.user.incomeLimit) {
                billsByDoctors.push(result)
              }
            }
          })
        }
      })
    }
  }
  return res.json({
    doctorNumbers: billsByDoctors.length,
    billsByBranch,
    billsByDoctors
  })
}
