const _ = require('lodash')
const Attendance = require('../../models/attendance')
const User = require('../../models/user')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

exports.makeAttendance = async (req, res, next) => {
  const schema = {
    user: Joi.objectId().required()
  }

  let result = Joi.validate(req.body, schema, {
    abortEarly: false
  })
  if (result.error) {
    res.status(400).json({
      type: 'error',
      message: result.error.details
    })
    return
  }
  const body = _.pick(req.body, ['user'])

  const user = await User.findById(body.user)
  if (!user) {
    const error = new Error('Could not find user.')
    error.statusCode = 404
    throw error
  }

  Attendance.create({ user: body.user }).then((attendance) => {
    Attendance.populate(attendance, { path: 'user' }).then((attendance) => {
      res.status(201).json({
        message: 'Attendance created successfully',
        data: attendance
      })
    })
  }).catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  })
}
