const mongoose = require('mongoose')
const setting = require('../lib/settings')
mongoose.set('useFindAndModify', false)

mongoose.connect(setting.mongodbServerUrl, { useNewUrlParser: true, useCreateIndex: true }).then(() => {
  console.log('Mongoose Connected')
}).catch((err) => {
  console.log('Something went wrong', err)
})
