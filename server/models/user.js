const mongoose = require('mongoose')
const Schema = mongoose.Schema
let timestamps = require('mongoose-timestamp')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const validator = require('validator')
const nodemailer = require('nodemailer')
const settings = require('../lib/settings')
// const config = require('config')

// const client = require('twilio')(config.get('accountSid'), config.get('authToken'))
const redis = require('redis');
const {RateLimiterRedis} = require('rate-limiter-flexible');

const userSchema = new Schema({
  number: {
    type: String,
    default: 0
  },
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: validator.isEmail
    }
  },
  password: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  image: {
    type: String,
    default: 'uploads/images/users/default.png'
  },
  birthdate: {
    type: Date,
    required: false
  },
  gender: {
    type: String,
    required: false
  },
  mobile: {
    type: String,
    required: false
    // unique: true
  },
  address: {
    type: String,
    required: false
  },
  tokens: [{
    access: {
      type: String,
      require: true
    },
    token: {
      type: String,
      require: true
    }
  }],
  isAdmin: {
    type: Boolean,
    default: false
  },
  active: {
    type: Boolean,
    default: false
  },
  confirmToken: {
    type: String,
    default: null
  },
  resetToken: {
    type: String,
    required: false
  },
  resetExpires: {
    type: Date,
    required: false
  },
  role: {
    type: mongoose.Types.ObjectId,
    ref: 'Role'
  },
  branch: {
    type: mongoose.Types.ObjectId,
    ref: 'Branch'
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  passportNumber: {
    type: String,
    default: null
  },
  passportStart: {
    type: Date,
    default: null
  },
  passportEnd: {
    type: Date,
    default: null
  },
  residenceNumber: {
    type: String,
    default: null
  },
  residenceStart: {
    type: Date,
    default: null
  },
  residenceEnd: {
    type: Date,
    default: null
  },
  workStart: {
    type: Date,
    default: null
  },
  workEnd: {
    type: Date,
    default: null
  },
  salary: {
    type: Number,
    default: 0
  },
  incomeLimit: {
    type: Number,
    default: null
  }
})

userSchema.methods.toJSON = function () {
  let user = this
  let userObject = user.toObject()
  delete userObject.tokens
  delete userObject.confirmToken
  if (user.image && user.image !== undefined) {
    userObject.image = `${settings.APP_URL}:${settings.apiListenPort}/` + user.image.replace(/\\/g, '/')
  } else {
    userObject.image = `${settings.APP_URL}:${settings.apiListenPort}/uploads/images/users/default.png`
  }
  return userObject
}

userSchema.methods.generateAuthToken = function () {
  let user = this
  let access = 'auth'
  let token = jwt.sign({
    _id: user._id.toHexString(),
    isAdmin: user.isAdmin,
    access}, settings.jwtSecretKey, {
      expiresIn: 15 * 24 * 60 * 60 * 1000
    }).toString()

  user.tokens = user.tokens.concat([{
    access,
    token
  }])
  return user.save().then(() => {
    return token
  })
}

userSchema.statics.findByToken = function (token) {
  let User = this
  let decoded

  try {
    decoded = jwt.verify(token, settings.jwtSecretKey)
  } catch (e) {
    // eslint-disable-next-line prefer-promise-reject-errors
    return Promise.reject(e)
  }

  return User.findOne({
    '_id': decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  })
}

userSchema.statics.removeToken = function (token, userId) {
  return this.updateOne({
    _id: userId
  }, {
    $pull: {
      tokens: {
        token: token
      }
    }
  })
}

userSchema.statics.findByCredentials = async function (email, password, statusCode = 404) {
  let User = this
  const redisClient = redis.createClient({
    host: 'localhost',
    port: 6379,
    enable_offline_queue: true
  });

  const maxConsecutiveFailsByUsername = 100;

  const limiterConsecutiveFailsByUsername = new RateLimiterRedis({
    redis: redisClient,
    keyPrefix: 'login_fail_consecutive_username',
    points: maxConsecutiveFailsByUsername,
    duration: 60 * 60 * 3, // Store number for three hours since first fail
    blockDuration: 60 * 15, // Block for 15 minutes
  });
  const username = email
  const rlResUsername = await limiterConsecutiveFailsByUsername.get(username)
  if (rlResUsername !== null && rlResUsername.remainingPoints <= 0) {
    const retrySecs = Math.round(rlResUsername.msBeforeNext / 1000) || 1
    const error = new Error(`Retry-After, ${String(retrySecs)} Too Many Invalid Credentials`)
    error.statusCode = 429
    throw error
  }
  else {
    return User.findOne({
      email
    }).then( async (user) => {
      if (!user || user.isDeleted) {
        await limiterConsecutiveFailsByUsername.consume(username)
        const error = new Error('A user with this email could not be found.')
        error.statusCode = statusCode
        throw error
      }
      return new Promise((resolve, reject) => {
        bcrypt.compare(password, user.password, async (err, res) =>{
          if (err) {
            await limiterConsecutiveFailsByUsername.consume(username)
            throw Error('something went wrong')
          }
          if (res) {
            await limiterConsecutiveFailsByUsername.delete(username)
            resolve(user)
          } else {
            await limiterConsecutiveFailsByUsername.consume(username)
            const error = new Error('Invalid Credentials')
            error.statusCode = statusCode
            reject(error)
          }
        })
      })
    }).catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500
      }
      throw err
    })
  }
}

userSchema.statics.sendEmail = function (user, attachements = null, data = null, html = null) {
  let options = {
    host: settings.smtpServer.host,
    port: settings.smtpServer.port,
    secure: false,
    auth: {
      user: settings.smtpServer.user,
      pass: settings.smtpServer.pass
    },
    ssl: {
      rejectUnauthorized: false
    },
    // eslint-disable-next-line node/no-unsupported-features/es-syntax
    ...(attachements ? {
      attachements
    } : {})
  }
  console.log('options', options)
  let transport = nodemailer.createTransport(options)

  if (!html) {
    html = 'Hello' + user.name + '<strong>, <br></br> You recently request a password expired in 1 hour your token is ' + user.resetToken
  }

  let mailOptions = {
    from: data ? data.from : `${settings.smtpServer.fromName} ${settings.smtpServer.fromAddress}`, // sender address
    to: user.email, // list of receivers
    subject: data ? data.subject : 'Activation Code ✔', // Subject line
    // text: data ? data.body : 'Hello world?', // plain text body
    html: html
  }
  transport.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error)
    }
    console.log('Message sent: %s', info.messageId)
    // Preview only available when sending through an Ethereal account
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info))

    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
  })
}

userSchema.methods.sendSms = function (user, message) {
  // client.messages.create({
  //     to: user.mobile,
  //     from: '+12672450597',
  //     body: message
  // }).then(message => console.log(message.sid)).done()
}

userSchema.pre('save', function (next) {
  let user = this
  if (!user.isModified('password')) {
    return next()
  }
  user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(8), null)
  next()
})

userSchema.statics.updatePassword = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
}

userSchema.post('save', function (error, doc, next) {
  if (error.name === 'MongoError' && error.code === 11000) {
    next(new Error('There was a duplicate key error'))
  } else {
    next()
  }
})

userSchema.post('update', function (error, res, next) {
  if (error.name === 'MongoError' && error.code === 11000) {
    next(new Error('There was a duplicate key error'))
  } else {
    next() // The `update()` call will still error out.
  }
})

userSchema.plugin(timestamps)

module.exports = mongoose.model('User', userSchema)
