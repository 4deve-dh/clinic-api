const mongoose = require('mongoose')
const Schema = mongoose.Schema
const nodemailer = require('nodemailer')

const reservationSchema = new Schema({
  patient: {
    type: mongoose.Types.ObjectId,
    ref: 'Patient',
    required: true
  },
  doctor: {
    type: mongoose.Types.ObjectId,
    ref: 'User',
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  time: {
    type: String,
    required: true
  },
  complain: {
    type: String,
    default: '',
    required: false
  },
  diagnosis: {
    type: String,
    default: '',
    required: false
  },
  status: {
    type: mongoose.Types.ObjectId,
    ref: 'Status',
    required: true
  },
  notes: {
    type: String,
    default: '',
    required: false
  },
  user: {
    type: mongoose.Types.ObjectId,
    ref: 'User',
    required: true
  },
  paymentType: {
    type: Array,
    default: []
  },
  paymentTypeValues: {
    type: Array,
    default: []
  },
  completed: {
    type: Boolean,
    default: false
  },
  branch: {
    type: mongoose.Types.ObjectId,
    ref: 'Branch',
    required: true
  },
  isDeleted: {
    type: Boolean,
    default: false
  }
}, {
  timestamps: true
})

reservationSchema.statics.sendEmail = function (user, reservation, attachements = null, data = null, html = null) {
  let options = {
    host: 'box.4deve.com',
    port: 587,
    secure: false,
    auth: {
      user: 'no-reply@4deve.com',
      pass: 'D3V3$Z3!9'
    },
    ssl: {
      rejectUnauthorized: false
    },
    ...(attachements ? { attachements } : {})
  }
  // let options = {
  //   host: 'smtp.gmail.com',
  //   port: 465,
  //   secure: true,
  //   auth: {
  //     user: 'anassmohamedons@gmail.com',
  //     pass: '6101992an'
  //   },
  //   ssl: {
  //     rejectUnauthorized: false
  //   },
  //   ...(attachements ? { attachements } : {})
  // }
  console.log('options', options)
  let transport = nodemailer.createTransport(options)

  if (!html) {
    html = 'Hello' + user.name_en + '<strong>, <br></br> You recently make reservation with docotr ' + reservation.doctor + 'at ' + reservation.data + 'time' + reservation.time
  }

  let mailOptions = {
    from: data ? data.from : '"Nawito" no-reply@4deve.com', // sender address
    to: user.email, // list of receivers
    subject: data ? data.subject : 'Activation Code ✔', // Subject line
    // text: data ? data.body : 'Hello world?', // plain text body
    html: html
  }
  transport.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error)
    }
    console.log('Message sent: %s', info.messageId)
    // Preview only available when sending through an Ethereal account
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info))

    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
  })
}

module.exports = mongoose.model('Reservation', reservationSchema)
