const mongoose = require('mongoose')
const Schema = mongoose.Schema

const roleSchema = new Schema({
    reservation: {
        type: mongoose.Types.ObjectId,
        ref: 'Reservation',
        required: true
    },
    // contractService: {
    //     type: mongoose.Types.ObjectId,
    //     ref: 'ContractService',
    //     required: true
    // },
    service: {
        type: mongoose.Types.ObjectId,
        ref: 'Service',
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    discount: {
        type: Number,
        default: 0
    },
}, { timestamps: true })

module.exports = mongoose.model('ReservationService', roleSchema)
