const mongoose = require('mongoose')
const Schema = mongoose.Schema

const contractSchema = new Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  responsible_name: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  isActive: {
    type: Boolean
  }
}, {
  timestamps: true
})

module.exports = mongoose.model('Contract', contractSchema)
