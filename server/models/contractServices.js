const mongoose = require('mongoose')
const Schema = mongoose.Schema

const contractServiceSchema = new Schema({
  contract: {
    type: mongoose.Types.ObjectId,
    ref: 'Contract',
    required: true
  },
  service: {
    type: mongoose.Types.ObjectId,
    ref: 'Service',
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  startDate: {
    type: Date,
    required: true
  },
  endDate: {
    type: Date,
    required: true
  }
}, {
  timestamps: true
})

module.exports = mongoose.model('ContractService', contractServiceSchema)
