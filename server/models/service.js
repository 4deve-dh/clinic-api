const mongoose = require('mongoose')
const Schema = mongoose.Schema

const serviceSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  branch: {
    type: mongoose.Types.ObjectId,
    ref: 'Branch',
    required: true
  },
  related: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Service',
    'default': []
  }],
  videoUrl: {
    type: String,
    default: null
  }
}, {
  timestamps: true
})

serviceSchema.methods.toJSON = function () {
  let service = this
  let serviceObject = service.toObject()
  if (service.videoUrl && service.videoUrl !== undefined) {
    serviceObject.videoUrl = 'http://192.168.1.24:3000/' + service.videoUrl.replace(/\\/g, '/')
  } else {
    serviceObject.videoUrl = null
  }
  return serviceObject
}

module.exports = mongoose.model('Service', serviceSchema)
