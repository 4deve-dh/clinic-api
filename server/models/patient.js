const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Reservation = require('./reservation')

const patientSchema = new Schema({
  number: {
    type: String,
    required: true
  },
  name_ar: {
    type: String,
    required: true
  },
  name_en: {
    type: String,
    required: true
  },
  age: {
    type: Date,
    required: true
  },
  gender: {
    type: String,
    required: true
  },
  mobile: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: false
  },
  notes: {
    type: String,
    required: false
  },
  nationality: {
    type: String,
    required: true
  },
  branch: {
    type: mongoose.Types.ObjectId,
    ref: 'Branch',
    required: true
  },
  // contract: {
  //   type: mongoose.Types.ObjectId,
  //   ref: 'Contract',
  //   required: true
  // },
  isDeleted: {
    type: Boolean,
    default: false
  },
  reservationsCount: {
    type: Number,
    default: 0
  },
  reservationsCountDate: {
    type: Date,
    default: null
  }
}, {
  timestamps: true
})

patientSchema.statics.patientExist = function (patientId) {
  const patient = this.findOne({
    _id: patientId
  })

  if (!patient) return false
  else return patient
}

patientSchema.methods.toJSON = function () {
  let patient = this
  let patientObject = patient.toObject()
  return patientObject
}

module.exports = mongoose.model('Patient', patientSchema)
