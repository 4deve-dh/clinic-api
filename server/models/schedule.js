const mongoose = require('mongoose')
const Schema = mongoose.Schema

const scheduleSchema = new Schema({
  doctor: {
    type: mongoose.Types.ObjectId,
    ref: 'User',
    required: true
  },
  day_of_week: {
    type: Number,
    required: true
  },
  start_time: {
    type: String,
    required: true
  },
  end_time: {
    type: String,
    required: true
  },
  branch: {
    type: mongoose.Types.ObjectId,
    ref: 'Branch',
    required: true
  }
}, {
  timestamps: true
})

module.exports = mongoose.model('Schedule', scheduleSchema)
