const mongoose = require('mongoose')
const Schema = mongoose.Schema

const vacationRequestSchema = new Schema({
  user: {
    type: mongoose.Types.ObjectId,
    ref: 'User',
    required: true
  },
  status: {
    type: String,
    required: true,
    default: 'requested'
  },
  start_date: {
    type: Date,
    required: true
  },
  end_date: {
    type: Date,
    required: true
  },
  note: {
    type: String,
    default: null
  },
  status_note: {
    type: String,
    default: null
  },
  branch: {
    type: mongoose.Types.ObjectId,
    ref: 'Branch',
    required: true
  }
}, {
  timestamps: true
})

module.exports = mongoose.model('VacationRequest', vacationRequestSchema)
