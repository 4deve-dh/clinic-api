const mongoose = require('mongoose')
const Schema = mongoose.Schema

const expenseDataSchema = new Schema({
  date: {
    type: Date,
    required: true
  },
  expense: {
    type: mongoose.Types.ObjectId,
    ref: 'Expense',
    required: true
  },
  cost: {
    type: Number,
    required: true
  },
  description: {
    type: String
  },
  branch: {
    type: mongoose.Types.ObjectId,
    ref: 'Branch',
    required: true
  },
}, {
  timestamps: true
})

module.exports = mongoose.model('ExpenseData', expenseDataSchema)