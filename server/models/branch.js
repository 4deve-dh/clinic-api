const mongoose = require('mongoose')
const Schema = mongoose.Schema

const branchSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: false
  },
  city: {
    type: String,
    required: false
  },
  profileStart: {
    type: Number
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  numberOfVacation: {
    type: Number,
    required: true
  },
  numberOfVacationMonth: {
    type: Number,
    required: true
  },
  cleanVacation: {
    type: Number,
    required: true
  },
  incomeLimit: {
    type: Number,
    required: true
  }
}, {
  timestamps: true
})

module.exports = mongoose.model('Branch', branchSchema)
