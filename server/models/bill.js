const mongoose = require('mongoose')
const Schema = mongoose.Schema

const billSchema = new Schema({
  number: {
    type: Number,
    required: true
  },
  reservation: {
    type: mongoose.Types.ObjectId,
    ref: 'Reservation',
    required: true
  },
  branch: {
    type: mongoose.Types.ObjectId,
    ref: 'Branch',
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  user: {
    type: mongoose.Types.ObjectId,
    ref: 'User',
    required: true
  },
  paidAmount: {
    type: Number,
    required: false,
    default: 0
  },
}, {
  timestamps: true
})

module.exports = mongoose.model('Bill', billSchema)
