const mongoose = require('mongoose')
const Schema = mongoose.Schema

const attendanceSchema = new Schema({
  user: {
    type: mongoose.Types.ObjectId,
    ref: 'User',
    required: true
  }
}, {
  timestamps: true
})

module.exports = mongoose.model('Attendance', attendanceSchema)
