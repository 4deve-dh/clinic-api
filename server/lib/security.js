const settings = require('./settings')
const getAccessControlAllowOrigin = () => {
  return settings.frontBaseUrl || '*'
}

module.exports = {
  getAccessControlAllowOrigin: getAccessControlAllowOrigin
}
