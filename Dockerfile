FROM node:latest
RUN npm install -g pm2
ENV NODE_ENV development
WORKDIR /var/www/public
COPY ["package.json", "pm2.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm install
RUN pm2 install pm2-auto-pull
COPY . .
EXPOSE 3000
CMD [ "pm2-runtime", "start", "pm2.json" ]